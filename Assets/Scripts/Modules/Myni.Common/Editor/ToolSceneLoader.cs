using UnityEditor;
using UnityEditor.SceneManagement;

namespace Myni.Common.Editor
{
    public static class ToolSceneLoader
    {
        private const string Tab = "/Load Scene";
        private const string ScenesPath = "Assets/Scenes";

        [MenuItem(CustomTools.ToolsTab + Tab + "/[0] Bootstrap")]
        private static void Bootstrap()
        {
            OpenScene($"{ScenesPath}/Loading/Bootstrap.unity");
        }
        
        [MenuItem(CustomTools.ToolsTab + Tab + "/[0.1] GDPR")]
        private static void GDPR()
        {
            OpenScene($"{ScenesPath}/Loading/GDPR.unity");
        }
        
        [MenuItem(CustomTools.ToolsTab + Tab + "/[0.2] MainLoading")]
        private static void MainLoading()
        {
            OpenScene($"{ScenesPath}/Loading/MainLoading.unity");
        }

        [MenuItem(CustomTools.ToolsTab + Tab + "/[1] MainMenu")]
        private static void MainMenu()
        {
            OpenScene($"{ScenesPath}/MainMenu.unity");
        }

        [MenuItem(CustomTools.ToolsTab + Tab + "/[2] CoreGame")]
        private static void CoreGame()
        {
            OpenScene($"{ScenesPath}/CoreGame.unity");
        }
        
        [MenuItem(CustomTools.ToolsTab + Tab + "/[3] TEST")]
        private static void Test()
        {
            OpenScene($"{ScenesPath}/Test.unity");
        }
        
        [MenuItem(CustomTools.ToolsTab + Tab + "/[4] Level Editor")]
        private static void LevelEditor()
        {
            OpenScene($"{ScenesPath}/LevelEditor.unity");
        }

        private static void OpenScene(string path)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(path);
        }
    }
}