﻿using UnityEngine;

namespace Modules.Myni.Common.App.Services.Cycle
{
    public class LateUpdater : BaseUpdater<ILateUpdatable>
    {
        private void LateUpdate()
        {
            DeltaTime = Time.deltaTime;

            foreach (var updatable in updatables)
            {
                if (updatable.IsDestroyed) continue;
                updatable.LateUpdateManually(DeltaTime);
            }

            ProcessUnregisterQueue();
            ProcessRegisterQueue();
        }
    }
}