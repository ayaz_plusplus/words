﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace StoneMaster.Core.Configs
{
    [CreateAssetMenu(menuName = "Core/Configuration/Level Letters Config")]
    public class LevelWordsConfig : SerializedScriptableObject
    {
        [field: OdinSerialize, BoxGroup("Words")]  public WordsByLevelConfig[] wordsForWinLevel { get; }
    }
}