using UnityEngine;

namespace Modules.Myni.Common.App.Services.Cycle
{
    public class RegularUpdater : BaseUpdater<IRegularUpdatable>
    {
        private void Update()
        {
            DeltaTime = Time.deltaTime;

            foreach (var updatable in updatables)
            {
                if (updatable.IsDestroyed) continue;
                updatable.UpdateManually(DeltaTime);
            }

            ProcessUnregisterQueue();
            ProcessRegisterQueue();
        }
    }
}