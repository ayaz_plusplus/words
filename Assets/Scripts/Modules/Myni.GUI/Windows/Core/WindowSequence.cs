﻿using System.Collections.Generic;
using System.Linq;

namespace Myni.GUI.Windows.Core
{
    public class WindowSequence
    {
        private readonly Stack<WindowBase> stack;

        public WindowSequence(int capacity)
        {
            stack = new Stack<WindowBase>(capacity);
        }

        public bool Any()
        {
            return stack.Any();
        }

        public WindowBase Pop()
        {
            return stack.Pop();
        }

        public void Push(WindowBase window)
        {
            stack.Push(window);
        }

        public bool TryPop(out WindowBase window)
        {
            if (Any())
            {
                window = stack.Pop();
                return true;
            }

            window = null;
            return false;
        }
    }
}