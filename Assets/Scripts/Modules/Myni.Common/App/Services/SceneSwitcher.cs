using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Myni.Common.App.Services
{
    public class SceneSwitcher : IAppService
    {
        [Inject] private CoroutineHost coroutineHost;

        private readonly WaitForSecondsRealtime delay;

        public SceneSwitcher(float delay)
        {
            this.delay = new WaitForSecondsRealtime(delay);
        }
        
        public void LoadScene(string sceneName)
        {
            coroutineHost.StartCoroutine(LoadSceneCoroutine(sceneName));
        }

        public IEnumerator LoadSceneAsync(string sceneName)
        {
            yield return coroutineHost.StartCoroutine(LoadSceneCoroutine(sceneName));
        }

        public void LoadSceneAdditive(string sceneName)
        {
            coroutineHost.StartCoroutine(LoadSceneAdditiveCoroutine(sceneName));
        }

        private IEnumerator LoadSceneCoroutine(string sceneName)
        {
            yield return delay;
            yield return SceneManager.LoadSceneAsync(sceneName);
            yield return delay;
        }

        private IEnumerator LoadSceneAdditiveCoroutine(string sceneName)
        {
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        }
    }
}