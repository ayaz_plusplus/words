﻿using UnityEngine;

namespace Modules.Myni.Common.App.Services.Cycle
{
    public abstract class UpdatableMonobehaviour : MonoBehaviour, IUpdatable
    {
        public bool IsDestroyed { get; private set; }

        protected virtual void OnDestroy()
        {
            IsDestroyed = true;
        }
    }
}