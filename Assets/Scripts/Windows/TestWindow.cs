using System.Collections.Generic;
using System.Threading.Tasks;
using Modules.Myni.GUI.Windows.Configs;
using Myni.GUI.Windows.Core;
using StoneMaster.UI;
using TMPro;
using UnityEngine.UI;

namespace Windows
{
    public class TestWindow : WindowBase
    {
        public TMP_Text head;
        public QuestionConfig[] questions;
        public ToggleGroup toggleGroup;
    
        private TestToggle[] toggles;
        private int currentStepIndex;
        private QuestionConfig currentQuestion;
        private List<string> answers = new List<string>();
        private void Awake()
        {
            InitializeQuestion();
        }

        private async void InitializeQuestion()
        {
            var task = TryNextStep();
            await task;
            
            if (!task.Result)
                return;
            
            currentQuestion = questions[currentStepIndex];
            head.text = currentQuestion.question;
            toggles = toggleGroup.GetComponentsInChildren<TestToggle>();
            for (var i = 0; i < toggles.Length; i++)
            {
                toggles[i].label.text = currentQuestion.answers[i];
            }
        }

        private async Task<bool> TryNextStep()
        {
            if (currentStepIndex <= questions.Length - 1)
                return true;
            
           var task = WindowShower.ShowAsync<RewardWindow>(WindowLayer.Foreground);
           await task;
           var rewardWindow = task.Result;
           rewardWindow.Initialize(answers);
           Close();
           return false;
        }

        public void NextQuestion()
        {
            currentStepIndex++;
            
            SaveAnswer();

            InitializeQuestion();
        }

        private void SaveAnswer()
        {
            var toggle = toggleGroup.GetFirstActiveToggle().GetComponent<TestToggle>();
            answers.Add(toggle.label.text);
        }
    }
}
