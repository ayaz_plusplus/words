﻿using System.Linq;
using StoneMaster.Core.Configs;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public abstract class SceneCube : CubeBase
    {
        [Inject] private Board board;
        [Inject] private GameConfig gameConfig;
        
        public void CellSetup(Cell cell)
        {
            Cell = cell;
            ResetTopLetterRotate();
        }
        
        public void NeighboursUpdate()
        {
            SearchBotNeighbour();
            SearchLeftNeighbour();
        }

        private void SearchBotNeighbour()
        {
            var letter = board.Letters
                .FirstOrDefault(letter => letter.Cell.Row == Cell.Row + 1 
                                          && letter.Cell.Column == Cell.Column);

            BottomNeighbour = letter;
        }
        
        private void SearchLeftNeighbour()
        {
            var letter = board.Letters
                .FirstOrDefault(letter => letter.Cell.Column == Cell.Column + 1
                                          && letter.Cell.Row == Cell.Row);
            
            LeftNeighbour = letter;

        }
        
        public void ResetTopLetterRotate()
        {
            CurrentLetter.transform.eulerAngles = Vector3
                .Lerp(transform.rotation.eulerAngles, 
                    gameConfig.TopLetterRotateDegrees, 1);
            
            NearestLetter.transform.eulerAngles = Vector3
                .Lerp(transform.rotation.eulerAngles, 
                    gameConfig.NearLetterRotateDegrees, 1);
            
            FarLetter.transform.eulerAngles = Vector3
                .Lerp(transform.rotation.eulerAngles, 
                    gameConfig.FarLetterRotateDegrees, 1);
        }
    }
}