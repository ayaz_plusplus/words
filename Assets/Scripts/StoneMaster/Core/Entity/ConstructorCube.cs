﻿namespace StoneMaster.Core.Entity
{
    public class ConstructorCube : CubeBase
    {
        public CubeType cubeType;
    }

    public enum CubeType
    {
        Moving,
        Static
    }
}