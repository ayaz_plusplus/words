﻿namespace Myni.Common.State
{
    public abstract class StateMachine : IStateMachine
    {
        public IState CurrentState { get; private set; }

        public void Initialize(IState state)
        {
            CurrentState = state;
            state.Enter();
        }
        
        public void Update()
        {
            CurrentState.Update();
        }

        public void SetState(IState state)
        {
            CurrentState.Exit();
            CurrentState = state;
            CurrentState.Enter();
        }
    }
}