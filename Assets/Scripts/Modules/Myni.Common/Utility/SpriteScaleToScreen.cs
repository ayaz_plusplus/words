﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Myni.Common.Utility
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteScaleToScreen : MonoBehaviour {
        [SerializeField] 
        private Camera currentCamera;
        
        [SerializeField, OnValueChanged("Stretch")]
        private bool stretchWidth;

        [SerializeField, OnValueChanged("Stretch")]
        private bool stretchHeight;

        [SerializeField, OnValueChanged("Stretch")]
        private float extraWidth;

        [SerializeField, OnValueChanged("Stretch")]
        private float extraHeight;

        private void Start()
        {
            Stretch();
        }

        [Button(ButtonSizes.Medium, ButtonStyle.Box)]
        private void Stretch()
        {
            if (Camera.main is { } || currentCamera)
            {
                var spriteSize = GetComponent<SpriteRenderer>().sprite.bounds.size;
                Vector2 edgeVector = (currentCamera ? currentCamera: Camera.main).ScreenToWorldPoint(Vector2.one);
                var width = Mathf.Abs(edgeVector.x * 2f) / spriteSize.x + extraWidth;
                var height = Mathf.Abs(edgeVector.y * 2f) / spriteSize.y + extraHeight;
                var transform1 = transform;
                var localScale = transform1.localScale;
                transform1.localScale = new Vector3(
                    stretchWidth ? width : stretchHeight ? height : localScale.x,
                    stretchHeight ? height : stretchWidth ? width : localScale.y,
                    localScale.z);
            }
        }
    }
}