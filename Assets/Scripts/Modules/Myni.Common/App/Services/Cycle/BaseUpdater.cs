﻿using System.Collections.Generic;
using UnityEngine;

namespace Modules.Myni.Common.App.Services.Cycle
{
    public abstract class BaseUpdater<T> : MonoBehaviour, IUpdater<T> where T : IUpdatable
    {
        protected readonly HashSet<T> updatables = new HashSet<T>();

        private readonly Queue<T> registerQueue = new Queue<T>();
        private readonly Queue<T> unregisterQueue = new Queue<T>();

        protected float DeltaTime { get; set; }

        public void Register(T regularUpdatable)
        {
            registerQueue.Enqueue(regularUpdatable);
        }

        public void Unregister(T regularUpdatable)
        {
            unregisterQueue.Enqueue(regularUpdatable);
        }

        protected void ProcessRegisterQueue()
        {
            while (registerQueue.Count > 0)
            {
                updatables.Add(registerQueue.Dequeue());
            }
        }

        protected void ProcessUnregisterQueue()
        {
            while (unregisterQueue.Count > 0)
            {
                updatables.Remove(unregisterQueue.Dequeue());
            }
        }
    }
}