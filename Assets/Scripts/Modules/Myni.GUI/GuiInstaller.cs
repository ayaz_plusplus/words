﻿using Modules.Windows.Configs;
using Myni.GUI.Windows.Core;
using UnityEngine;
using Zenject;

namespace Myni.GUI
{
    [CreateAssetMenu(menuName = "Myni/GUI/GUI Installer")]
    public class GuiInstaller : ScriptableObjectInstaller<GuiInstaller>
    {
        public const string MainCanvasId = "MainCanvas";

        [SerializeField] private Canvas mainCanvasPrefab;
        [SerializeField] private UserInterfaceConfig userInterfaceConfig;

        public override void InstallBindings()
        {
            Container.BindInstance(userInterfaceConfig);

            Container.Bind<WindowsContainer>().AsSingle();
            Container.Bind<WindowShower>().AsSingle();
            Container.BindInterfacesAndSelfTo<WindowFocuser>().AsSingle();

            Container.Bind<IAssetReferenceGameObjectProvider>().To<WindowAssetReferenceGameObjectProvider>().AsCached();

            var mainCanvasInstance = Instantiate(mainCanvasPrefab);
            DontDestroyOnLoad(mainCanvasInstance);
            Container.BindInstance(mainCanvasInstance).WithId(MainCanvasId).AsCached();
            Container.BindInstance(mainCanvasInstance).WhenInjectedInto<WindowsContainer>();

            Container.BindFactory<WindowBase, WindowBase, WindowBase.Factory>()
                .FromFactory<PrefabFactory<WindowBase>>();
        }
    }
}