﻿using System;

namespace Myni.GUI.Windows.Core
{
    public class WindowEventArgs : EventArgs
    {
        public WindowBase window;
        public WindowLayer windowLayer;
    }
}