using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Myni.GUI.Components
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class HyperlinkOpener : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private TMP_Text targetText;

        private void Reset()
        {
            targetText = GetComponent<TMP_Text>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var linkIndex = TMP_TextUtilities.FindIntersectingLink(targetText, eventData.position, null);
            if (linkIndex < 0) return;

            var linkInfo = targetText.textInfo.linkInfo[linkIndex];
            Application.OpenURL(linkInfo.GetLinkID());
        }
    }
}