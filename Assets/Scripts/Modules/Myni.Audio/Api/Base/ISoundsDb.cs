namespace Myni.Audio.Api.Base
{
    public interface ISoundsDb
    {
        bool TryGetSoundMeta(string id, out SoundMeta soundMeta);
    }
}