﻿using DG.Tweening;

namespace Modules.Windows.Configs
{
    public interface IWindowAnimationConfig
    {
        float AnimationDuration { get; }
        Ease Ease { get; }
    }
}