﻿using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class TransformExtensions
    {
        public static T GetComponentInChildrenFirstDepth<T>(this Transform transform)
        {
            foreach (Transform trans in transform)
            {
                if (trans.TryGetComponent(out T component))
                    return component;
            }

            return default;
        }
    }
}