using Myni.Audio.Api;
using Myni.Audio.Api.Base;
using UnityEngine;
using Zenject;

namespace Myni.Audio
{
    public class SoundComponent : MonoBehaviour
    {
        [SerializeField] private string soundId;
        [SerializeField] private SoundChannel soundChannel = SoundChannel.Sfx;

        [Inject] private IAudioPlayer audioPlayer;

        public void Play()
        {
            audioPlayer.Play(soundId, soundChannel);
        }
    }
}