﻿using System;
using System.Collections.Generic;
using HCG.MatchMaster.Core.GameStates.States;
using Myni.Common.State;
using Zenject;

namespace StoneMaster.Core.GameStates
{
    public class GameStateMachine : StateMachine
    {
        private readonly Dictionary<Type, GameState> gameStates = new Dictionary<Type, GameState>();

        [Inject] private GameIdleState.Factory gameIdleStateFactory;
        [Inject] private GameLevelSpawnState.Factory gameLevelSpawnStateFactory;
        [Inject] private GameLevelClearState.Factory gameClearLevelStateFactory;
        [Inject] private GamePlayingState.Factory gamePlayingStateFactory;
        [Inject] private GameMetaIdleState.Factory gameMetaIdleStateFactory;

     //   [Inject] public SignalBus SignalBus { get; }

        public void Initialize()
        {
            var idleState = gameIdleStateFactory.Create(this);
            AddState(idleState);
            AddState(gameClearLevelStateFactory.Create(this));
            AddState(gameLevelSpawnStateFactory.Create(this));
            AddState(gamePlayingStateFactory.Create(this));
            AddState(gameMetaIdleStateFactory.Create(this));
            base.Initialize(idleState);
        }

        private void AddState<T>(T gameState) where T : GameState
        {
            gameStates[typeof(T)] = gameState;
        }

        public void SetState<T>() where T : GameState
        {
            if (gameStates.TryGetValue(typeof(T), out var state))
            {
                SetState(state);
            }
        }

        public class Factory : PlaceholderFactory<GameStateMachine>
        {
        }
    }
}