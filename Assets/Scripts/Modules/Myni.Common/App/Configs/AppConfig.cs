using UnityEngine;

namespace Myni.Common.App.Configs
{
    [CreateAssetMenu(menuName = "HCG/Common/App/Configs/App Config")]
    public class AppConfig : ScriptableObject
    {
        [SerializeField] private float fakeLoadingTime = 1f;
        [SerializeField][TextArea] private string buildInfo;

        public float FakeLoadingTime => fakeLoadingTime;
        public string BuildInfo => buildInfo;
    }
}