﻿using System;
using System.Collections.Generic;
using System.Linq;
using Myni.GUI.Windows.Core;
using TMPro;

namespace Windows
{
    public class RewardWindow : WindowBase
    {
        public TMP_Text head;
        private List<string> answers;

        public void Initialize(List<string> answers)
        {
            this.answers = answers;
            head.text = answers.First();
        }
    }
}