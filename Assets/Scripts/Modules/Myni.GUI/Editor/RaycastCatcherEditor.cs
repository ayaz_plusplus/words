using Myni.GUI.Components;
using UnityEditor;
using UnityEditor.UI;

namespace Myni.GUI.Editor
{
    [CanEditMultipleObjects, CustomEditor(typeof(RaycastCatcher), false)]
    public class RaycastCatcherEditor : GraphicEditor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(m_Script);
            RaycastControlsGUI();
            serializedObject.ApplyModifiedProperties();
        }
    }
}