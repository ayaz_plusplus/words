﻿using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Myni.GUI.Components
{
    public class TimeredButton : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private GameObject buttonHolder;
        [SerializeField] private GameObject timerHolder;
        [SerializeField] private TextMeshProUGUI timerText;

        private bool isTimered;
        private DateTime unlockDateTime;

        private void OnEnable()
        {
            UpdateState();
        }

        private void OnDisable()
        {
            isTimered = false;
        }

        public void SetUnlockTime(DateTime dateTime)
        {
            unlockDateTime = dateTime;
            UpdateState();
        }

        private void UpdateState()
        {
            if (IsUnlocked())
            {
                buttonHolder.SetActive(true);
                timerHolder.SetActive(false);
                button.interactable = true;
            }
            else
            {
                buttonHolder.SetActive(false);
                timerHolder.SetActive(true);
                button.interactable = false;

                StartTimerTask();
            }
        }

        private async void StartTimerTask()
        {
            isTimered = true;

            while (isTimered)
            {
                var diff = unlockDateTime - DateTime.UtcNow;
                timerText.text = diff.ToString(@"hh\:mm\:ss");

                if (diff < TimeSpan.Zero)
                {
                    UpdateState();
                    isTimered = false;
                }

                await Task.Delay(1000);
            }
        }

        private bool IsUnlocked()
        {
            return DateTime.UtcNow > unlockDateTime;
        }
    }
}