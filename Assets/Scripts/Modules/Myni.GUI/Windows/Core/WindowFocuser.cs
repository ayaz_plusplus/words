﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Myni.GUI.Windows.Core
{
    public class WindowFocuser : IInitializable
    {
        [Inject] private WindowsContainer windowsContainer;

        public event Action<WindowBase> OnWindowFocus;

        private readonly List<WindowBase> openedWindows;

        private WindowBase currentFocusedWindow;

        public WindowFocuser()
        {
            openedWindows = new List<WindowBase>();
        }

        public void Initialize()
        {
            windowsContainer.OnWindowAdd += OnWindowAdd;
            windowsContainer.OnWindowRemove += OnWindowRemove;
        }

        private void OnWindowAdd(WindowLayer windowLayer, WindowBase window)
        {
            if (windowLayer == WindowLayer.MainOverlay) return;

            openedWindows.Add(window);
            FocusLast();
        }

        private void OnWindowRemove(WindowBase window)
        {
            window.Defocus();
            if (openedWindows.Remove(window))
            {
                FocusLast();
            }
        }

        private void FocusLast()
        {
            if (currentFocusedWindow) currentFocusedWindow.Defocus();
            if (openedWindows.Count < 1) return;

            currentFocusedWindow = openedWindows[openedWindows.Count - 1];
            currentFocusedWindow.Focus();
            OnWindowFocus?.Invoke(currentFocusedWindow);
        }

        ~WindowFocuser()
        {
            if (windowsContainer == null) return;
            windowsContainer.OnWindowAdd -= OnWindowAdd;
            windowsContainer.OnWindowRemove -= OnWindowRemove;
        }
    }
}