﻿using System;
using Sirenix.Serialization;

namespace StoneMaster.Core.Configs
{
    public class WordsByLevelConfig
    {
        [field: OdinSerialize]  public int Level;
        [field: OdinSerialize]  public string[] Words;
    }
}