using System.Threading.Tasks;
using Windows;
using Myni.GUI.Windows.Core;
using StoneMaster.Core.Entity;
using StoneMaster.Core.GameStates;
using StoneMaster.Core.GameStates.Pause;
using Zenject;

namespace HCG.MatchMaster.Core.GameStates.States
{
    public class GamePlayingState : GameState
    {
        [Inject] private SignalBus signalBus;
        [Inject] private IPauseHandler pauseHandler;
        [Inject] private Board board;
        [Inject] private WindowShower windowShower;
        
        private CoreHud coreHud;
        public GamePlayingState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }

        public override async void Enter()
        {
            await ShowCoreHud();

            pauseHandler.Resume();
        }

        public override void Update()
        {
        }

        public override void Exit()
        {

        }

        private async Task ShowCoreHud()
        {
            coreHud = await windowShower.ShowAsync<CoreHud>(WindowLayer.Main);
            board.Initialize(coreHud);
        }
        
        private void OnPauseGame()
        {

        }

        public class Factory : PlaceholderFactory<GameStateMachine, GamePlayingState>
        {
        }
    }
}