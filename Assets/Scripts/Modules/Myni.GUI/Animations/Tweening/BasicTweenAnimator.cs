﻿using DG.Tweening;
using UnityEngine;

namespace Myni.GUI.Animations.Tweening
{
    public abstract class BasicTweenAnimator : MonoBehaviour
    {
        [SerializeField] private Ease ease;
        [SerializeField] private int loops = 1;
        [SerializeField] private LoopType loopType = LoopType.Incremental;
        [SerializeField] private bool unscaledTime = true;
        [SerializeField] protected float duration;

        private float valueCurrent;
        private float valueTo;

        private Tween tween;

        protected abstract void Animate(float t);

        public void Toggle(bool value)
        {
            valueTo = value ? 1f : 0f;
            if (tween is { active: true })
            {
                tween.Kill();
            }

            tween = DOTween.To(ValueGetter, ValueSetter, valueTo, duration)
                .SetUpdate(unscaledTime)
                .SetEase(ease)
                .SetLoops(loops, loopType)
                .Play();
        }

        private void OnDestroy()
        {
            tween?.Kill();
        }

        public void ForceAnimate(float t)
        {
            valueCurrent = t;
            Animate(t);
        }

        private float ValueGetter()
        {
            return valueCurrent;
        }

        private void ValueSetter(float t)
        {
            valueCurrent = t;
            Animate(t);
        }
    }
}