using Modules.Myni.Common.App.Services.Cycle;
using UnityEngine;
using Zenject;

namespace Myni.Common.Utility
{
    public class ShaderUnscaledTimeWrapper : UpdatableMonobehaviour, IRegularUpdatable
    {
        private readonly int unscaledTime = Shader.PropertyToID("_UnscaledTime");

        [Inject] private IUpdater<IRegularUpdatable> updater;

        private void OnEnable()
        {
            updater.Register(this);
        }

        private void OnDisable()
        {
            updater.Unregister(this);
        }

        public void UpdateManually(float deltaTime)
        {
            Shader.SetGlobalFloat(unscaledTime, 30f + Time.unscaledTime);
        }
    }
}