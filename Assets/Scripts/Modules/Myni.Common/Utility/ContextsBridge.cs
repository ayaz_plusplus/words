﻿using System;
using Zenject;

namespace Myni.Common.Utility
{
    public class ContextsBridge
    {
        [Inject] private SignalBus signalBus;
        
        public void Subscribe<T>(Action<T> signal)
        {
            signalBus.Subscribe(signal);
        }

        public void Unsubscribe<T>(Action<T> signal)
        {
            signalBus.Unsubscribe(signal);
        }
    }
}