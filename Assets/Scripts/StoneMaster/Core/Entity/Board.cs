﻿using System.Collections.Generic;
using Windows;
using Myni.Common.User.Profile.Api.Data;
using StoneMaster.Core.Configs;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public class Board : MonoBehaviour
    {
        [SerializeField] private District district;

        public District District => district;

        [Inject] private MovingCube.Factory movingCubeFactory;
        [Inject] private StaticCube.Factory staticCubeFactory;
        [Inject] private LevelWordsConfig levelWordsConfig;
        [Inject] private LevelProgressData levelProgressData;
        
        private CoreHud coreHud;
        private WordsByLevelConfig wordsByLevelConfig;
        private List<SceneCube> letters = new List<SceneCube>();
        
        public List<SceneCube> Letters => letters;
        public WordsByLevelConfig WordsByLevelConfig => wordsByLevelConfig;
        
        public void Initialize(CoreHud coreHud)
        {
            this.coreHud = coreHud;
            district.Initialize(this);

            if (levelWordsConfig.wordsForWinLevel.Length > levelProgressData.CurrentLevel)
            {
                wordsByLevelConfig = levelWordsConfig.wordsForWinLevel[levelProgressData.CurrentLevel];

                if (coreHud.Word == false)
                    return;
                
                Debug.Log($"wordsByLevelConfig.Words.Length {wordsByLevelConfig.Words.Length} word {wordsByLevelConfig.Words[0]} " +
                          $"coreGameHud {coreHud} coreGameHud.Word.text {coreHud.Word.text}");
            
                coreHud.Word.text = wordsByLevelConfig.Words[0];
            }
        }

        public void CreateLetter(Cell cell, Transform parent, CubeType cubeType, LettersContainer constructorLetters)
        {
            SceneCube letter;

            switch (cubeType)
            {
                case CubeType.Static: 
                    letter = staticCubeFactory.Create();
                    break;
                case CubeType.Moving: 
                    letter = movingCubeFactory.Create();
                    break;
                default:
                    letter = staticCubeFactory.Create();
                    break;
            }

            var letterTransform = letter.transform;
            letterTransform.SetParent(parent);

            letter.CellSetup(cell);
            letter.transform.localPosition = new Vector3(0, 0, -.5f);

            letter.LettersContainer.SetLetters(constructorLetters.Letters); 
            letters.Add(letter);
        }

        public void Clear()
        {
            foreach (var letter in letters)
                Destroy(letter.gameObject);
            
            letters.Clear();
        }


        public class Factory : PlaceholderFactory<Board>
        {
        }
    }
}