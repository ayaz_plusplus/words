﻿using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class VectorExtensions
    {
        public static Vector2 ToXZ(this Vector3 vec)
        {
            return new Vector2(vec.x, vec.z);
        }
        
        public static Vector2 ToXY(this Vector3 vec)
        {
            return new Vector2(vec.x, vec.y);
        }

        public static Vector3 Clamp(this Vector3 vec, float min, float max)
        {
            return new Vector3(
                Mathf.Clamp(vec.x, min, max),
                Mathf.Clamp(vec.y, min, max),
                Mathf.Clamp(vec.z, min, max));
        }

        public static Vector3 HalfCircle(this Vector3 vec)
        {
            return new Vector3(vec.x > 180f ? vec.x - 360f : vec.x,
                vec.y > 180f ? vec.y - 360f : vec.y,
                vec.z > 180f ? vec.z - 360f : vec.z);
        }

        public static Vector3 ScreenToWorld(float x, float y, float z = 0)
        {
            Camera camera = Camera.main;
            return camera.ScreenToWorldPoint(new Vector3(x, camera.pixelHeight - y, z));
        }

        public static Rect ScreenRect(int x, int y, int w, int h)
        {
            Vector3 tl = ScreenToWorld(x, y);
            Vector3 br = ScreenToWorld(x + w, y + h);
            return new Rect(tl.x, tl.y, br.x - tl.x, br.y - tl.y);
        }
    }
}