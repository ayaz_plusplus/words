﻿namespace StoneMaster.SceneManagement
{
    public enum SceneType
    {
        Bootstrap,
        GDPR,
        MainLoading,
        MainMenu,
        CoreGame,
        TutorialGameplay
    }
}