﻿using System;

namespace Myni.Common.Utility
{
    public class EventWrapper
    {
        private event Action Action;

        public void Invoke()
        {
            Action?.Invoke();
        }

        public void AddListener(Action listener)
        {
            Action += listener;
        }

        public void RemoveListener(Action listener)
        {
            Action -= listener;
        }
    }
}