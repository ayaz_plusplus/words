namespace Modules.Myni.Common.App.Services.Cycle
{
    public interface IUpdatable : IDestroyable
    {
    }

    public interface IDestroyable
    {
        bool IsDestroyed { get; }
    }

    public interface IRegularUpdatable : IUpdatable
    {
        void UpdateManually(float deltaTime);
    }

    public interface IFixedUpdatable : IUpdatable
    {
        void FixedUpdateManually(float fixedDeltaTime);
    }

    public interface ILateUpdatable : IUpdatable
    {
        void LateUpdateManually(float deltaTime);
    }
}