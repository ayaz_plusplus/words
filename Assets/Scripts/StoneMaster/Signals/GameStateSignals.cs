﻿namespace StoneMaster.Signals
{
    public class GoToMetaSignal
    {
        public bool OnPlaying = false;

        public GoToMetaSignal()
        {
        }

        public GoToMetaSignal(bool onPlaying)
        {
            OnPlaying = onPlaying;
        }

    }
    
    public class GoToPlaySignal
    {
        public GoToPlaySignal()
        {
        }
    }
    
    public class RestartedSignal
    {
        public RestartedSignal()
        {
        }
    }
}