﻿using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class CameraExtensions
    {
        public static Vector2 ScreenToPlanePoint(this Camera cam, Plane plane, Vector2 screenPos)
        {
            var ray = cam.ScreenPointToRay(screenPos);

            return plane.Raycast(ray, out var enter) ? ray.GetPoint(enter).ToXZ() : Vector2.zero;
        }
    }
}