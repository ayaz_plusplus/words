﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class DictionaryExtensions
    {
        public static IEnumerable<TValue> RandomValues<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            var values = dict.Values.ToList();
            var size = dict.Count;

            while (true) yield return values[Random.Range(0, size)];
        }

        public static IEnumerable<TValue> RandomValues<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dict)
        {
            var values = dict.Values.ToList();
            var size = dict.Count;

            while (true) yield return values[Random.Range(0, size)];
        }
    }
}