﻿using DG.Tweening;

namespace Myni.GUI.Windows.Core.Animation
{
    public interface IWindowAnimator : IKillable
    {
        void Show(TweenCallback onShowingAnimationEnd);
        void Hide(TweenCallback yieldClose);
        void Skip();
    }
}