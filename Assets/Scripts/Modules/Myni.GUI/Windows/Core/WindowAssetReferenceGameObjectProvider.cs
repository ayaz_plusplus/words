﻿using Modules.Windows.Configs;
using UnityEngine.AddressableAssets;
using Zenject;

namespace Myni.GUI.Windows.Core
{
    public class WindowAssetReferenceGameObjectProvider : IAssetReferenceGameObjectProvider
    {
        [Inject] private UserInterfaceConfig userInterfaceConfig;

        public bool TryGet(System.Type type, out AssetReferenceGameObject result)
        {
            return userInterfaceConfig.TryGetAssetReference(type, out result);
        }
    }
}