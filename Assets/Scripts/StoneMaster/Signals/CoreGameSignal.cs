﻿using StoneMaster.Core.Entity;

namespace StoneMaster.Signals
{
    public class CoreGameSignal
    {
        public class LetterSelectedSignal
        {
            public MovingCube MovingCube;

            public LetterSelectedSignal(MovingCube movingCube)
            {
                MovingCube = movingCube;
            }
        }
        
        public class LetterUnselectedSignal
        {
            public MovingCube MovingCube;

            public LetterUnselectedSignal(MovingCube movingCube)
            {
                MovingCube = movingCube;
            }
        }
    }
}