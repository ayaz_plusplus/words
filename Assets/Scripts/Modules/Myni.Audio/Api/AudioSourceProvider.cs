using System.Collections.Generic;
using Myni.Audio.Api.Base;
using Myni.Audio.Utility;
using UnityEngine;

namespace Myni.Audio.Api
{
    public class AudioSourceProvider : IAudioSourceProvider
    {
        private readonly Transform container;
        private readonly Dictionary<SoundChannel, AudioSourcePool> audioSourcePools;

        public AudioSourceProvider()
        {
            audioSourcePools = new Dictionary<SoundChannel, AudioSourcePool>();
            container = new GameObject("[Audio] AudioSource.Container").transform;
            Object.DontDestroyOnLoad(container);
            
            EnumUtility.ForEach<SoundChannel>(CreateAudioSourcePool);
        }

        private void CreateAudioSourcePool(SoundChannel soundChannel)
        {
            audioSourcePools[soundChannel] = new AudioSourcePool(container);
        }

        bool IAudioSourceProvider.TryGetAudioSource(SoundChannel soundChannel, out AudioSource audioSource)
        {
            audioSource = audioSourcePools[soundChannel].Get();
            return true;
        }
    }
}