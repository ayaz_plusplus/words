﻿using UnityEngine;

namespace Myni.GUI.Animations.Tweening
{
    public class TransformLocalPositionTweener : BasicTweenAnimator
    {
        [SerializeField] private Transform target;
        [SerializeField] private Vector3 positionFrom;
        [SerializeField] private Vector3 positionTo;

        protected override void Animate(float t)
        {
            target.localPosition = Vector3.Lerp(positionFrom, positionTo, t);
        }
    }
}