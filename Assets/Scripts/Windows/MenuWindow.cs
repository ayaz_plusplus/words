﻿using Myni.GUI.Windows.Core;
using StoneMaster.Signals;
using Zenject;

namespace Windows
{
    public class MenuWindow : WindowBase
    {
        [Inject] private SignalBus signalBus;

        public void FindStone()
        {
            signalBus.Fire<GoToPlaySignal>();
            Close();
        }
    }
}