﻿using UnityEngine;

namespace Myni.Common.Utility
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class CameraWidthMatcher : MonoBehaviour
    {
        [SerializeField] private float sceneWidth = 10;

        private Camera targetCamera;

        private void Start()
        {
            targetCamera = GetComponent<Camera>();
        }

        private void Update()
        {
            var unitsPerPixel = sceneWidth / Screen.width;

            var desiredHalfHeight = 0.5f * unitsPerPixel * Screen.height;

            targetCamera.orthographicSize = desiredHalfHeight;
        }
    }
}