﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;

namespace StoneMaster.Core.Entity
{
    public abstract class CubeBase : MonoBehaviour
    {
        public Cell Cell;
        public CubeBase BottomNeighbour { get; protected set; }
        public CubeBase LeftNeighbour { get; protected set; }

        private LettersContainer lettersContainer;

        public LettersContainer LettersContainer => lettersContainer;

        public TMP_Text CurrentLetter => lettersContainer
            .Letters
            .OrderByDescending(letter => letter.transform.position.y)
            .FirstOrDefault();

        public TMP_Text NearestLetter => lettersContainer
            .Letters
            .OrderBy(letter => letter.transform.position.z)
            .FirstOrDefault();
        
        public TMP_Text FarLetter => lettersContainer
            .Letters
            .OrderByDescending(letter => letter.transform.position.x)
            .FirstOrDefault();
        
      private void Awake()
      {
          lettersContainer = GetComponent<LettersContainer>();
      }
    }
}