using Zenject;

namespace StoneMaster.Core.GameStates
{
    public class GameIdleState : GameState
    {
        public GameIdleState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }

        public override void Enter()
        {
            
        }

        public override void Update()
        {

        }

        public override void Exit()
        {
        }

        public class Factory : PlaceholderFactory<GameStateMachine, GameIdleState>
        {
        }
    }
}