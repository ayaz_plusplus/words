#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
#define DEBUG_LEVEL_LOADING
#endif

using System.Collections;
using HCG.MatchMaster.Core.GameStates.States;
using Myni.Common.App.Services;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.GameStates
{
    public class GameLevelSpawnState : GameState
    {
        [Inject] private CoroutineHost coroutineHost;
        [Inject] private SignalBus signalBus;

        private Coroutine loadLevelRoutine;

        public GameLevelSpawnState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }

        public override void Enter()
        {

        }

        private void OnLeaveCore()
        {
            if (loadLevelRoutine != null)
            {
                coroutineHost.StopCoroutine(loadLevelRoutine);
            }
        }

        private void StartPlaying()
        {

        }

        public override void Update()
        {
        }

        public override void Exit()
        {
        }

        public class Factory : PlaceholderFactory<GameStateMachine, GameLevelSpawnState>
        {
        }
    }
}