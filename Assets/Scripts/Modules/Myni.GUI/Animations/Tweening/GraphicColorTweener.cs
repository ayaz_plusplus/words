﻿using UnityEngine;
using UnityEngine.UI;

namespace Myni.GUI.Animations.Tweening
{
    public class GraphicColorTweener : BasicTweenAnimator
    {
        [SerializeField] private Graphic targetGraphic;
        [SerializeField] private Color colorFrom;
        [SerializeField] private Color colorTo;

        protected override void Animate(float t)
        {
            targetGraphic.color = Color.Lerp(colorFrom, colorTo, t);
        }
    }
}