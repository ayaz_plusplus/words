﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class CollectionExtensions
    {
        public static T Shuffle<T>(this T list) where T : IList
        {
            var count = list.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i)
            {
                var r = Random.Range(i, count);
                (list[i], list[r]) = (list[r], list[i]);
            }

            return list;
        }

        public static IEnumerable<T> RandomValues<T>(this IEnumerable<T> collection)
        {
            var values = collection.ToList();
            var size = values.Count;

            while (true) yield return values[Random.Range(0, size)];
        }

        public static T GetRandomItem<T>(this IEnumerable<T> collection)
        {
            var values = collection.ToList();
            var size = values.Count;

            return values[Random.Range(0, size)];
        }

        public static T GetNext<T>(this IList<T> collection, int index)
        {
            return index < collection.Count ? collection[index] : collection[0];
        }

        public static Stack<T> Shuffle<T>(this Stack<T> stack)
        {
            List<T> list = stack.ToList();
            list.Shuffle();
            return list.ToStack();
        }

        public static Stack<T> ToStack<T>(this List<T> list)
        {
            Stack<T> stack = new Stack<T>();
            foreach (T t in list)
                stack.Push(t);

            return stack;
        }
    }
}