using System.Threading.Tasks;
using UnityEngine;

namespace Myni.Common.User.Profile.Api.Data
{
    [System.Serializable]
    public abstract class ProfileData
    {
        protected string Name { get; private set; }

        protected bool IsSaving { get; set; }

        public abstract void SaveImmediately();
        public abstract void Reset();
        public abstract void OnGenerate();
        public abstract void OnLoaded();

        internal void SetFilename(string generatedName)
        {
            Name = generatedName;
        }

        public async void Save()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                Debug.LogWarning($"Can't save data without name! {GetType().Name}");
                return;
            }

            while (IsSaving) await Task.Delay(25);

            SaveImmediately();
        }
    }
}