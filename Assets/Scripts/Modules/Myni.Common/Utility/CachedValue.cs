﻿using UnityEngine;

namespace Myni.Common.Utility
{
    public abstract class CachedValue<T>
    {
        private T value;

        private bool hasValue;
        
        public T Value
        {
            get
            {
                if (hasValue) return value;
                value = CalculateValue();
                hasValue = true;
                return value;
            }
            
            protected set => this.value = value;
        }

        protected abstract T CalculateValue();
    }

    public class CachedTransformPoint : CachedValue<Vector3>
    {
        protected override Vector3 CalculateValue()
        {
            return Vector3.zero;
        }
    }
}