﻿using System;
using System.Collections.Generic;
using Myni.GUI.Extensions;
using UnityEngine;

namespace Myni.GUI.Windows.Core
{
    public class WindowsContainer
    {
        private readonly Dictionary<int, WindowBase> windows;
        private readonly Dictionary<WindowLayer, RectTransform> layers;
        private readonly Transform transform;

        public event Action<WindowLayer, WindowBase> OnWindowAdd;
        public event Action<WindowBase> OnWindowRemove;

        // ReSharper disable once SuggestBaseTypeForParameter
        internal WindowsContainer(Canvas mainCanvas)
        {
            windows = new Dictionary<int, WindowBase>();
            layers = new Dictionary<WindowLayer, RectTransform>();
            transform = mainCanvas.transform;

            InitializeLayers();
        }

        private void InitializeLayers()
        {
            var layerValues = Enum.GetValues(typeof(WindowLayer));
            foreach (var layer in layerValues) CreateLayer((WindowLayer) layer);
        }

        private void CreateLayer(WindowLayer windowLayer)
        {
            var layerGameObject = new GameObject(windowLayer.ToString()) {layer = LayerMask.NameToLayer("UI")};
            var rectTransform = layerGameObject.AddComponent<RectTransform>();
            rectTransform.SetParent(transform);
            rectTransform.StretchToParent();
            layers[windowLayer] = rectTransform;
        }

        public void Add(WindowLayer windowLayer, WindowBase window)
        {
            var type = window.GetType();
            var hashCode = type.GetHashCode();

            if (windows.ContainsKey(hashCode))
                return;

            if (!layers.TryGetValue(windowLayer, out var layerRectTransform))
            {
                Debug.LogError($"Couldn't get layer: {windowLayer}");
                return;
            }

            window.Initialize();

            window.RectTransform.SetParent(layerRectTransform);
            window.RectTransform.StretchToParent();

            windows[hashCode] = window;
            OnWindowAdd?.Invoke(windowLayer, window);
        }

        public void Add(RectTransform holder, WindowBase window)
        {
            var type = window.GetType();
            var hashCode = type.GetHashCode();

            if (windows.ContainsKey(hashCode))
                return;
            
            window.Initialize();

            window.RectTransform.SetParent(holder);
            window.RectTransform.StretchToParent();

            windows[hashCode] = window;
        }        
        
        public void Remove(WindowBase window)
        {
            if (windows.Remove(window.GetType().GetHashCode()))
            {
                OnWindowRemove?.Invoke(window);
            }
        }

        public bool TryGetWindow(int key, out WindowBase windowBase)
        {
            return windows.TryGetValue(key, out windowBase);
        }

        public bool TryGetWindow<T>(out T window) where T : WindowBase
        {
            if (TryGetWindow(typeof(T).GetHashCode(), out var windowBase))
            {
                window = (T) windowBase;
                return true;
            }

            window = null;
            return false;
        }

        public bool ContainsKey(int hashCode)
        {
            return windows.ContainsKey(hashCode);
        }

        public bool Contains<T>() where T : WindowBase
        {
            return ContainsKey(typeof(T).GetHashCode());
        }
    }
}