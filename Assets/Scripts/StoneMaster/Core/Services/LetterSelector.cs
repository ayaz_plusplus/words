﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lean.Touch;
using Modules.Myni.Common.App.Services.Cycle;
using StoneMaster.Core.Configs;
using StoneMaster.Core.Entity;
using StoneMaster.Signals;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Services
{
    public class LetterSelector : IInitializable, IDisposable
    {
        [Inject] private SignalBus signalBus;
        [Inject] private GameConfig gameConfig;
        private List<MovingCube> selectedLetters = new List<MovingCube>();
        private int maxSelectedLettes = 1;

        public void Initialize()
        {
            signalBus.Subscribe<CoreGameSignal.LetterSelectedSignal>(OnSelected);
            signalBus.Subscribe<CoreGameSignal.LetterUnselectedSignal>(OnUnselected);
            signalBus.Subscribe<RestartedSignal>(Clear);
            LeanTouch.OnFingerTap += LeanTouchOnOnFingerTap;
        }

        public void Dispose()
        {
            signalBus.TryUnsubscribe<CoreGameSignal.LetterSelectedSignal>(OnSelected);
            signalBus.TryUnsubscribe<CoreGameSignal.LetterUnselectedSignal>(OnUnselected);
            signalBus.TryUnsubscribe<RestartedSignal>(Clear);
            LeanTouch.OnFingerTap -= LeanTouchOnOnFingerTap;
        }

        private void LeanTouchOnOnFingerTap(LeanFinger obj)
        {
            MovingCube selectedCube;
            if ((selectedCube = Select(obj.ScreenPosition)) != null)
            {
                selectedCube.OnTouchCube();
            }
        }
        
        private MovingCube Select(Vector3 position)
        {
            Ray raycast = Camera.main.ScreenPointToRay(position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit,Mathf.Infinity, gameConfig.LetterLayerMask))
            {
                if (raycastHit.collider.gameObject.TryGetComponent<MovingCube>(out var component))
                    return component;
            }

            return null;
        }
        
        private void Clear()
        {
            foreach (var letter in selectedLetters.ToList())
            {
                letter.UnSelect();
            }
            selectedLetters.Clear();
        }
        
        private void OnUnselected(CoreGameSignal.LetterUnselectedSignal signal)
        {
            if (selectedLetters.Contains(signal.MovingCube))
                selectedLetters.Remove(signal.MovingCube);
        }
        
        private void OnSelected(CoreGameSignal.LetterSelectedSignal signal)
        {
            if (selectedLetters.Contains(signal.MovingCube))
                return;
            
            if (selectedLetters.Count >= maxSelectedLettes)
            {
                Clear();
            }
      
            selectedLetters.Add(signal.MovingCube);
        }
    }
}