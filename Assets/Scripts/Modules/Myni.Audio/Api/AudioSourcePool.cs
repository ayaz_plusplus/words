using System.Collections.Generic;
using UnityEngine;

namespace Myni.Audio.Api
{
    public class AudioSourcePool
    {
        private readonly Transform container;

        private readonly Stack<AudioSource> audioSources;

        private readonly LinkedList<AudioSource> inUseAudioSources = new LinkedList<AudioSource>();
        private readonly Queue<LinkedListNode<AudioSource>> nodePool = new Queue<LinkedListNode<AudioSource>>();

        private int lastCheckFrame = -1;
        private int count;

        public AudioSourcePool(Transform container)
        {
            this.container = container;
            audioSources = new Stack<AudioSource>();
        }

        public AudioSource Get()
        {
            if (lastCheckFrame != Time.frameCount)
            {
                lastCheckFrame = Time.frameCount;
                CheckInUse();
            }

            var availableAudioSource = audioSources.Count > 0 ? audioSources.Pop() : Create();

            MarkInUse(availableAudioSource);

            return availableAudioSource;
        }

        private void CheckInUse()
        {
            var node = inUseAudioSources.First;
            while (node != null)
            {
                var current = node;
                node = node.Next;

                if (current.Value.isPlaying) continue;

                audioSources.Push(current.Value);
                inUseAudioSources.Remove(current);
                nodePool.Enqueue(current);
            }
        }

        private AudioSource Create()
        {
            var gameObject = new GameObject($"AudioSource{++count}");
            gameObject.transform.SetParent(container);
            return gameObject.AddComponent<AudioSource>();
        }

        private void MarkInUse(AudioSource audioSource)
        {
            if (nodePool.Count == 0)
            {
                inUseAudioSources.AddLast(audioSource);
            }
            else
            {
                var node = nodePool.Dequeue();
                node.Value = audioSource;
                inUseAudioSources.AddLast(node);
            }
        }
    }
}