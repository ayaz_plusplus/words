﻿using UnityEngine.EventSystems;

namespace Myni.Common.App.Services
{
    public class EventSystemHolder
    {
        public EventSystem EventSystem { get; set; }
    }
}