﻿using System;
using System.Linq;
using System.Text;
using Myni.Common.User.Profile.Api.Data;
using StoneMaster.Core.Configs;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public class WordChecker
    {
        [Inject] private Board board;
        private string[] currentWords => board.WordsByLevelConfig?.Words;
        
        public void SearchWords()
        {
            NeighboursUpdate();

            SearchWordInRows();
            SearchWordInColumns();
        }

        private void SearchWordInRows()
        {
            SearchWord(GetColumnKey(), GetRowKey(),  GetBottomNeighbour);
        }
        
        private void SearchWordInColumns()
        {
            SearchWord(GetRowKey(), GetColumnKey(), GetLeftNeighbour);
        }

        private void SearchWord(Func<SceneCube,int> groupSelector, Func<SceneCube, int> sortSelector, Func<CubeBase, CubeBase> getNeighbour)
        {
            foreach (var group in board
                         .Letters
                         .GroupBy(groupSelector))
            {
                var leftLetters = @group
                    .OrderBy(sortSelector);

                foreach (var letter in leftLetters)
                {
                    CubeBase nextLeftNeighbour = letter;
                    StringBuilder word = new StringBuilder();
                    while (nextLeftNeighbour != null)
                    {
                        word.Append(nextLeftNeighbour.CurrentLetter.text);

                        nextLeftNeighbour = getNeighbour.Invoke(nextLeftNeighbour);
                    }

                    if (word.Length > 1)
                    {
                        Debug.Log($"Find word {word}");
                        if (currentWords.Contains(word.ToString()))
                        {
                            Debug.Log($"Win()");
                        }
                    }
                       
                }
            }
        }
        
        private CubeBase GetBottomNeighbour(CubeBase cubeBase)
        {
            return cubeBase.BottomNeighbour;
        }
        
        private CubeBase GetLeftNeighbour(CubeBase cubeBase)
        {
            return cubeBase.LeftNeighbour;
        }
        
        private Func<SceneCube, int> GetColumnKey()
        {
            return (cube) => cube.Cell.Column;
        }
        
        private Func<SceneCube,int> GetRowKey()
        {
            return (cube) => cube.Cell.Row;
        }
        
        
        private void NeighboursUpdate()
        {
            foreach (var letter in board.Letters)
                letter.NeighboursUpdate();
        }
    }
}