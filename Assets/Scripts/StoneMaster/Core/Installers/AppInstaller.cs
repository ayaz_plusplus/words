﻿
using Modules.Myni.Common.App.Services.Cycle;
using Modules.Windows.Configs;
using Myni.Common.App.Services;
using Myni.Common.User.Profile.Api;
using Myni.Common.User.Profile.Api.Data;
using StoneMaster.Core.Configs;
using StoneMaster.Core.Entity;
using StoneMaster.Core.GameStates.Pause;
using StoneMaster.Core.Services;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace StoneMaster.Core.Installers
{
    public class AppInstaller : MonoInstaller<AppInstaller>
    {
        [SerializeField] private GameObject servicesPrefab;
        [SerializeField] private UserInterfaceConfig userInterfaceConfig;

        private EventSystemHolder eventSystemHolder;
        public override void InstallBindings()
        {
            Container.Bind<IUpdater<IRegularUpdatable>>().To<RegularUpdater>().FromNewComponentOnNewGameObject().AsSingle();
            Container.Bind<IUpdater<ILateUpdatable>>().To<LateUpdater>().FromNewComponentOnNewGameObject().AsSingle();
            Container.Bind<IUpdater<IFixedUpdatable>>().To<FixedUpdater>().FromNewComponentOnNewGameObject().AsSingle();
            Container.Bind<IPauseHandler>().To<SimplePauseHandler>().AsCached();
            Container.Bind<SceneSwitcher>().AsSingle();
            Container.BindInstance(userInterfaceConfig.AnimationDuration).WhenInjectedInto<SceneSwitcher>();
            LoadProfile(Container);
        }
        
        private void Awake()
        {
            InstallServices();
        }
        
        private static void LoadProfile(DiContainer container)
        {
            BindLocalData<LevelProgressData>(container, "level");
        }
        
        private static void BindLocalData<T>(DiContainer container, string name) where T : ProfileData, new()
        {
            container.BindInstance(Profile.InitLocalData<T>(name)).AsCached();
        }
        
        private void InstallServices()
        {
            var servicesGameObject = Container.InstantiatePrefab(servicesPrefab);
            var services = servicesGameObject.GetComponentsInChildren<IAppService>(true);

            foreach (var appService in services)
            {
                Debug.Log($"InstallServices {appService}");
                Container.Bind(appService.GetType()).FromInstance(appService);
            }

     //       eventSystemHolder.EventSystem = servicesGameObject.GetComponentInChildren<EventSystem>();

            /*Container.BindInterfacesTo<ZenUnitPrefabProvider>().AsSingle();
            Container.BindInterfacesAndSelfTo<TilePoolProvider>().AsSingle();
            Container.BindInterfacesAndSelfTo<UnitPoolProvider>().AsSingle();*/
        }

    }
}