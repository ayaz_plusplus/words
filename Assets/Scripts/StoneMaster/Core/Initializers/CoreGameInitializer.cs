﻿using Myni.GUI.Windows.Core;
using StoneMaster.Signals;
using Zenject;

namespace StoneMaster.Core.Initializers
{
    public class CoreGameInitializer : SceneInitializer
    {
        [Inject] private WindowShower windowShower;
        [Inject] private SignalBus signalBus;

        private void Start()
        {
            signalBus.Fire<GoToMetaSignal>();
        }
    }
}