using UnityEngine;
using UnityEngine.UI;

namespace Myni.GUI.Components
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class RaycastCatcher : Graphic
    {
        public override void SetMaterialDirty()
        {
        }

        public override void SetVerticesDirty()
        {
        }

        /// Probably not necessary since the chain of calls
        /// `Rebuild()`->
        /// `UpdateGeometry()`->
        /// `DoMeshGeneration()`->
        /// `OnPopulateMesh()`
        /// won't happen; so here really just as a fail-safe.
        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();
        }
    }
}