﻿using System;

namespace Myni.Common.Utility
{
	public static class TimeUtility
	{
		public static long UnixTime => DateTimeOffset.Now.ToUnixTimeSeconds();
		public static long UnixTimeMills => DateTimeOffset.Now.ToUnixTimeMilliseconds();
		public static long UnixTimeUtc => DateTimeOffset.UtcNow.ToUnixTimeSeconds();
	}
}