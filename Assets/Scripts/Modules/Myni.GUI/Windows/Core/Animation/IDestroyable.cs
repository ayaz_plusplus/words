﻿namespace Myni.GUI.Windows.Core.Animation
{
    public interface IKillable
    {
        void Kill();
    }
}