﻿using Myni.Common.App.Configs;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Installers
{
    [CreateAssetMenu(menuName = "HCG/Installers/Loading Installer")]
    public class LoadingInstaller : ScriptableObjectInstaller<LoadingInstaller>
    {
        [SerializeField] private LoadingConfig loadingConfig;

        public override void InstallBindings()
        {
            Container.BindInstance(loadingConfig);
        }
    }
}