﻿using UnityEngine;

namespace Myni.Common.Utility
{
    public static class RaycastUtility
    {
        public static Vector3 ScreenToPlanePoint(this Camera cam, Plane plane, Vector3 screenPos)
        {
            var ray = cam.ScreenPointToRay(screenPos);

            return plane.Raycast(ray, out var enter) ? ray.GetPoint(enter) : Vector3.zero;
        }
    }
}