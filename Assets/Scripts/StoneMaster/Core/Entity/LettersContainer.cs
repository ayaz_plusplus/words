﻿using TMPro;
using UnityEngine;

namespace StoneMaster.Core.Entity
{
    public class LettersContainer : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text[] letters;

        public TMP_Text[] Letters => letters;

        public void SetLetters(TMP_Text[] newLetters)
        {
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].text = newLetters[i].text;
            }
        }
    }
}