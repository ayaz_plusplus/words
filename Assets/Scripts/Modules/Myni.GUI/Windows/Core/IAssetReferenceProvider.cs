﻿using UnityEngine.AddressableAssets;

namespace Myni.GUI.Windows.Core
{
    public interface IAssetReferenceGameObjectProvider
    {
        bool TryGet(System.Type type, out AssetReferenceGameObject result);
    }
}