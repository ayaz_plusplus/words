﻿using UnityEngine;

namespace Myni.GUI.Animations.Tweening
{
    public class RectTransformPositionTweener : BasicTweenAnimator
    {
        [SerializeField] private RectTransform target;
        [SerializeField] private Vector3 positionFrom;
        [SerializeField] private Vector3 positionTo;

        protected override void Animate(float t)
        {
            target.anchoredPosition = Vector3.Lerp(positionFrom, positionTo, t);
        }
    }
}