namespace Myni.Audio.Api.Base
{
    public interface IAudioPlayer
    {
        void Play(string id, SoundChannel soundChannel);
        bool Stop(string id, SoundChannel soundChannel);
    }
}