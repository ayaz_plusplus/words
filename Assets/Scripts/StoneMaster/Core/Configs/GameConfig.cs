﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace StoneMaster.Core.Configs
{
    [CreateAssetMenu(menuName = "Core/Configuration/Game Config")]
    public class GameConfig : SerializedScriptableObject
    {
        [Title("Letter"), BoxGroup("Letter")]
        
        [field: OdinSerialize, BoxGroup("Technical")] public float RoolSpeed { get; }
        [field: OdinSerialize, BoxGroup("Technical")] public float AfterMoveDelay{ get; }
        
        [field: OdinSerialize, BoxGroup("Technical")] public float AnchorOffset { get; }
        
        [field: OdinSerialize, BoxGroup("Technical")] public LayerMask LetterLayerMask { get; }
        
        [field: OdinSerialize, BoxGroup("Technical")] public Material SelectedCube { get; }
        [field: OdinSerialize, BoxGroup("Technical")] public Material Cube { get; }
        
        [field: OdinSerialize, BoxGroup("Selector")] public Vector3 TopLetterRotateDegrees { get; }
        [field: OdinSerialize, BoxGroup("Selector")] public Vector3 NearLetterRotateDegrees { get; }
        
        [field: OdinSerialize, BoxGroup("Selector")] public Vector3 FarLetterRotateDegrees { get; }
    }
}