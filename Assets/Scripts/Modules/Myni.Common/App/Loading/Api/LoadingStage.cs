#if !UNITY_EDITOR
#define LOADING_TIME_MEASURE
#endif

using System.Collections;
using UnityEngine;
using Zenject;

namespace Myni.Common.App.Loading.Api
{
    public abstract class LoadingStage : ScriptableObject
    {
        [Inject] private SignalBus signalBus;

#if LOADING_TIME_MEASURE
        private double loadTimeMs;
#endif
        
        public abstract IEnumerator Load();

        protected void StartLoad()
        {
#if LOADING_TIME_MEASURE
            loadTimeMs = Time.realtimeSinceStartupAsDouble;
#endif
        }

        protected void EndLoad()
        {
#if LOADING_TIME_MEASURE
            Debug.Log($"{name}: {Time.realtimeSinceStartupAsDouble - loadTimeMs}");
#endif

            signalBus.Fire<StageLoadedSignal>();
        }
    }
}