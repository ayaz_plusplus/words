﻿using System;
using System.Threading.Tasks;
using Modules.Windows.Configs;
using Myni.GUI.Components;
using Myni.GUI.Extensions;
using Myni.GUI.Windows.Core.Animation;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Myni.GUI.Windows.Core
{
    [RequireComponent(typeof(CanvasGroup), typeof(Canvas), typeof(GraphicRaycaster))]
    public class WindowBase : MonoBehaviour
    {
        public event Action<WindowState> StateChanged;
        public event Action OnClose;

        [SerializeField] private RectTransform animatedRectTransform;
        [SerializeField] private bool useDarkener = true;

        [Inject] private WindowShower windowShower;
        [Inject] private UserInterfaceConfig userInterfaceConfig;
        [Inject] private WindowsContainer windowsContainer;

        private WindowState currentState;
        private IWindowAnimator windowAnimator;

        protected CanvasGroup canvasGroup;

        public RectTransform RectTransform { get; private set; }

        protected RectTransform TargetRectTransform => animatedRectTransform ? animatedRectTransform : RectTransform;
        public GameObject GameObject => gameObject;
        public Transform Transform => transform;

        public bool UseDarkener => useDarkener;

        public WindowState CurrentState
        {
            get => currentState;
            private set
            {
                currentState = value;
                StateChanged?.Invoke(currentState);
            }
        }

        protected UserInterfaceConfig UserInterfaceConfig => userInterfaceConfig;

        protected WindowShower WindowShower => windowShower;
        protected WindowsContainer WindowsContainer => windowsContainer;

        public virtual void Reset()
        {
            CreateOrSetSafeArea();
        }

        public virtual void OnDestroy()
        {
            windowAnimator?.Kill();
        }

        public virtual void Initialize()
        {
            RectTransform = GetComponent<RectTransform>();
            RectTransform.localScale = Vector3.zero;

            canvasGroup = GetComponent<CanvasGroup>();
            windowAnimator = GetWindowAnimator();
        }

        public virtual IWindowAnimator GetWindowAnimator()
        {
            return new FadeWindowAnimator(TargetRectTransform, userInterfaceConfig.FadeAnimationConfig, canvasGroup);
        }

        public virtual void Focus()
        {
        }

        public virtual void Defocus()
        {
        }

        public virtual void Close()
        {
            OnClose?.Invoke();

            CloseWithoutNotify();
        }

        protected void CloseWithoutNotify()
        {
            if (this == null) return;

            canvasGroup.interactable = false;
            CurrentState = WindowState.Closing;
            windowAnimator.Hide(YieldClose);
            windowShower.Close(this);
        }

        public void Hide()
        {
            canvasGroup.interactable = false;
            CurrentState = WindowState.Hiding;
            windowAnimator.Hide(OnAnimationHide);
        }

        public virtual void Show()
        {
            if (!gameObject.activeSelf) gameObject.SetActive(true);
            CurrentState = WindowState.Opening;
            windowAnimator.Show(OnShowingAnimationEnd);
        }

        private void OnShowingAnimationEnd()
        {
            CurrentState = WindowState.Opened;
            canvasGroup.interactable = true;
        }

        private async void YieldClose()
        {
            CurrentState = WindowState.Closed;
            await Task.Yield();
            windowShower.OnClosed(this);
        }

        private void OnAnimationHide()
        {
            CurrentState = WindowState.Hidden;
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }

        public void SkipAnimation()
        {
            windowAnimator.Skip();
        }        
        
        private void CreateOrSetSafeArea()
        {
            var safeArea = GetComponentInChildren<UISafeArea>()?.gameObject;

            if (safeArea == null)
            {
                safeArea = new GameObject("SafeArea");
                safeArea.transform.SetParent(transform);
                var safeAreaRectTransform = safeArea.AddComponent<RectTransform>();
                safeAreaRectTransform.StretchToParent();
                animatedRectTransform = safeAreaRectTransform;
                safeArea.AddComponent<UISafeArea>();
            }
            else
            {
                if (!safeArea.TryGetComponent(out RectTransform safeAreaRectTransform))
                {
                    safeAreaRectTransform = safeArea.AddComponent<RectTransform>();
                }

                safeAreaRectTransform.StretchToParent();
                animatedRectTransform = safeAreaRectTransform;
            }
        }

        public class Factory : PlaceholderFactory<WindowBase, WindowBase>
        {
        }
    }
}