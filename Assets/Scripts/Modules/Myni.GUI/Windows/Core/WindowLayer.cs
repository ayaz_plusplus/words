﻿namespace Myni.GUI.Windows.Core
{
    public enum WindowLayer
    {
        Main,

        // For Main OVERLAY! not WINDOWS! For windows use Foreground
        MainOverlay, // Special additional like-window panel

        Background,
        Foreground,
        Effects,
        OverEffects,
        Debug
    }
}