﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Myni.GUI.Windows.Core
{
    public class WindowShowSequence
    {
        private readonly WindowShower windowShower;
        private readonly Queue<Task> queue;

        public WindowShowSequence(WindowShower windowShower)
        {
            this.windowShower = windowShower;
            queue = new Queue<Task>(5);
        }

        private async void Run()
        {
            while (queue.Count > 0)
            {
                await queue.Dequeue();
            }
        }

        public WindowShowSequence AppendShow<T>(WindowLayer windowLayer, Action<T> callback = null) where T : WindowBase
        {
            queue.Enqueue(GetShowTask(windowLayer, callback));
            return this;
        }

        public WindowShowSequence AppendCallback(Action callback)
        {
            queue.Enqueue(GetCallbackTask(callback));
            return this;
        }

        public WindowShowSequence RunNextFrame()
        {
            Task.Run(Run);
            return this;
        }

        private async Task GetShowTask<T>(WindowLayer windowLayer, Action<T> callback = null) where T : WindowBase
        {
            var task = windowShower.ShowAsync<T>(windowLayer);
            await task;
            callback?.Invoke(task.Result);
        }

        private static async Task GetCallbackTask(Action callback)
        {
            callback.Invoke();
            await Task.Yield();
        }
    }
}