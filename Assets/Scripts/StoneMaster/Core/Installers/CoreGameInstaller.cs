﻿using HCG.MatchMaster.Core.GameStates.States;
using StoneMaster.Core.Configs;
using StoneMaster.Core.Entity;
using StoneMaster.Core.GameStates;
using StoneMaster.Core.Services;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Installers
{
    public class CoreGameInstaller : MonoInstaller<CoreGameInstaller>
    {
        [SerializeField] private Board boardPrefab;
        [SerializeField] private MovingCube movingCubePrefab;
        [SerializeField] private StaticCube staticCubePrefab;
        [SerializeField] private GameConfig gameConfig;
        [SerializeField] private LevelWordsConfig levelWordsConfig;
        
        public override void InstallBindings()
        {
            Container.BindInstance(levelWordsConfig).AsCached();
            Container.BindInstance(gameConfig).AsCached();
            Container.BindFactory<GameStateMachine, GameStateMachine.Factory>();
            Container.BindFactory<GameStateMachine, GameIdleState, GameIdleState.Factory>();
            Container.BindFactory<GameStateMachine, GamePlayingState, GamePlayingState.Factory>();
            Container.BindFactory<GameStateMachine, GameLevelSpawnState, GameLevelSpawnState.Factory>();
            Container.BindFactory<GameStateMachine, GameLevelClearState, GameLevelClearState.Factory>();
            Container.BindFactory<GameStateMachine, GameMetaIdleState, GameMetaIdleState.Factory>();
            
            Container.BindInterfacesAndSelfTo<LetterSelector>().AsCached();
            Container.BindInterfacesAndSelfTo<WordChecker>().AsCached();
            InstallBoard();
        }
        
                
        private void InstallBoard()
        {
            Container.BindFactory<MovingCube, MovingCube.Factory>().FromComponentInNewPrefab(movingCubePrefab);
            Container.BindFactory<StaticCube, StaticCube.Factory>().FromComponentInNewPrefab(staticCubePrefab);
            Container.BindFactory<Board, Board.Factory>().FromComponentInNewPrefab(boardPrefab);
            var boardFactory = Container.Resolve<Board.Factory>();
            var boardInstance = boardFactory.Create();
            DontDestroyOnLoad(boardInstance);
            Container.BindInstance(boardInstance).AsCached();
        }
    }
}