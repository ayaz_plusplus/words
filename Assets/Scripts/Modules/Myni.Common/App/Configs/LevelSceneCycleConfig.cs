﻿using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace Myni.Common.App.Configs
{
    [CreateAssetMenu(menuName = "HCG/Common/App/Configs/Level Scene Cycle Config")]
    public class LevelSceneCycleConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [SerializeField] private SceneAsset[] levelScenes;
#endif

        [SerializeField] private string[] levels;

        public string GetLevelSceneName(int index)
        {
            return levels[index % levels.Length];
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            levels = new string[levelScenes.Length];

            for (var i = 0; i < levelScenes.Length; ++i)
            {
                levels[i] = levelScenes[i] == null ? string.Empty : levelScenes[i].name;
            }
        }
#endif
    }
}