﻿using UnityEngine;

namespace Myni.Common.Utility.FlyValue
{
    [CreateAssetMenu(menuName = "HCG/Common/Utility/FlyValue/Float")]
    public class FlyFloat : FlyValueBase<float>
    {
    }
}