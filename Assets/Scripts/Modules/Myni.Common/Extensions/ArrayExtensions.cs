﻿using System;

namespace Myni.Common.Extensions
{
    public static class ArrayExtensions
    {
        private static readonly Random Rng = new Random();

        public static T[] Shuffled<T>(this T[] source)
        {
            var shuffled = new T[source.Length];
            source.CopyTo(shuffled, 0);

            var len = source.Length;
            for (var i = 0; i < len - 1; i++)
            {
                var j = i + Rng.Next(len - i);
                var t = shuffled[j];
                shuffled[j] = shuffled[i];
                shuffled[i] = t;
            }

            return shuffled;
        }

        public static T Random<T>(this T[] source)
        {
            return source[UnityEngine.Random.Range(0, source.Length)];
        }
    }
}