#if !UNITY_EDITOR
#define USE_ENCRYPTION
#endif

#if USE_ENCRYPTION
using System.Text;
#endif

using System.IO;
using Myni.Common.User.Profile.Api.Data;
using Newtonsoft.Json;

namespace Myni.Common.User.Profile.Api
{
    public static class Profile
    {
        public const string DataFormat =
#if USE_ENCRYPTION
            ".data";
#else
            ".json";
#endif

#if USE_ENCRYPTION
        private const string EncryptKey = "nKPZBbTB@UMishqFC$53";
#endif

        public static bool FirstLaunch { get; set; }

        public static T InitLocalData<T>(string name) where T : ProfileData, new()
        {
            return InitData<T>(ProfilePathProvider.GetLocalPath(), name);
        }

        public static T InitServerData<T>(string name) where T : ProfileData, new()
        {
            return InitData<T>(ProfilePathProvider.GetServerPath(), name);
        }

        private static T InitData<T>(string path, string name) where T : ProfileData, new()
        {
            T profileData;
            var filePath = $"{path}/{name}{DataFormat}";

            if (Directory.Exists(path) == false) Directory.CreateDirectory(path);

            if (File.Exists(filePath))
            {
                profileData = Load<T>(filePath);
                profileData.SetFilename(name);
                profileData.OnLoaded();
            }
            else
            {
                profileData = new T();
                profileData.SetFilename(name);
                profileData.OnGenerate();
                profileData.OnLoaded();
                profileData.Save();
            }

            return profileData;
        }

        public static void Save<T>(T data, string path)
        {
            using var streamWriter = new StreamWriter(path);
            var content = JsonConvert.SerializeObject(data, Formatting.Indented);

#if USE_ENCRYPTION
            streamWriter.Write(EncryptOrDecrypt(content));
#else
			streamWriter.Write(content);
#endif
        }

        private static T Load<T>(string path)
        {
            using var streamReader = new StreamReader(path);
            var content = streamReader.ReadToEnd();

#if USE_ENCRYPTION
            return JsonConvert.DeserializeObject<T>(EncryptOrDecrypt(content));
#else
			return JsonConvert.DeserializeObject<T>(content);
#endif
        }

#if USE_ENCRYPTION
        private static string EncryptOrDecrypt(string text)
        {
            var result = new StringBuilder();

            for (var c = 0; c < text.Length; c++)
            {
                result.Append((char)(text[c] ^ (uint)EncryptKey[c % EncryptKey.Length]));
            }

            return result.ToString();
        }
#endif
    }
}