namespace StoneMaster.Core.GameStates.Pause
{
    public interface IPauseHandler
    {
        void Pause();
        void Resume();
    }
}