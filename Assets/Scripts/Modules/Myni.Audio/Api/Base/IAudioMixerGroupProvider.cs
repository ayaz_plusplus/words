using UnityEngine.Audio;

namespace Myni.Audio.Api.Base
{
    public interface IAudioMixerGroupProvider
    {
        bool TryGet(SoundChannel soundChannel, out AudioMixerGroup audioMixerGroup);
    }
}