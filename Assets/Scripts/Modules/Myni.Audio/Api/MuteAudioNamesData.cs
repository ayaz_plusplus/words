using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Myni.Audio.Api
{
    [CreateAssetMenu(menuName = "HCG/Common/Audio/FMOD/Mute Audio Names Data")]
    public class MuteAudioNamesData : SerializedScriptableObject
    {
        [OdinSerialize] private Dictionary<SoundChannel, string> muteAudioNamesDictionary;
        
        public bool TryGet(SoundChannel soundChannel, out string muteAudioName)
        {
            return muteAudioNamesDictionary.TryGetValue(soundChannel, out muteAudioName);
        }        
    }
}
