﻿using UnityEngine;
using Zenject;

namespace StoneMaster.Core.GameStates
{
    public class GameLevelClearState : GameState
    {



        public GameLevelClearState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }

        public override void Enter()
        {

        }

        public override void Update()
        {
        }

        public override void Exit()
        {
        }

        public class Factory : PlaceholderFactory<GameStateMachine, GameLevelClearState>
        {
        }
    }
}