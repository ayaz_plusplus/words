﻿using UnityEngine;

namespace Myni.GUI.Animations.Tweening
{
    public class AnimationActivator : MonoBehaviour
    {
        [SerializeField] private BasicTweenAnimator[] tweenAnimators;

        public void Toggle(bool value)
        {
            foreach (var tweenAnimator in tweenAnimators) tweenAnimator.Toggle(value);
        }

        public void ForceAnimate(float f)
        {
            foreach (var tweenAnimator in tweenAnimators) tweenAnimator.ForceAnimate(f);
        }
    }
}