using UnityEngine;
using Zenject;

namespace StoneMaster.Core.GameStates.Pause
{
    public class SimplePauseHandler : IPauseHandler
    {
        [Inject] private SignalBus signalBus;

        public void Pause()
        {
            Time.timeScale = 0f;
          //  signalBus.Fire<PauseGameSignal>();
        }

        public void Resume()
        {
            Time.timeScale = 1f;
          //  signalBus.Fire<ResumeGameSignal>();
        }
    }
}