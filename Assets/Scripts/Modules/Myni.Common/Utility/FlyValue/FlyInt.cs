﻿using UnityEngine;

namespace Myni.Common.Utility.FlyValue
{
    [CreateAssetMenu(menuName = "HCG/Common/Utility/FlyValue/Int")]
    public class FlyInt : FlyValueBase<int>
    {
    }
}