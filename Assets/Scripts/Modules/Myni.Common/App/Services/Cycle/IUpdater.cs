namespace Modules.Myni.Common.App.Services.Cycle
{
    public interface IUpdater<T> where T : IUpdatable
    {
        void Register(T regularUpdatable);
        void Unregister(T regularUpdatable);
    }
}