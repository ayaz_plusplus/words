using System.IO;

namespace Myni.Common.User.Profile.Api.Data
{
	[System.Serializable]
	public abstract class LocalProfileData : ProfileData
	{
		public override void SaveImmediately()
		{
            if (!Directory.Exists(ProfilePathProvider.GetLocalPath()))
            {
                return;
            }

            var path = $"{ProfilePathProvider.GetLocalPath()}/{Name}{Profile.DataFormat}";

            IsSaving = true;

			Profile.Save(this, path);

			IsSaving = false;
        }

		public override void Reset()
		{
			OnGenerate();
			Save();
		}
	}
}