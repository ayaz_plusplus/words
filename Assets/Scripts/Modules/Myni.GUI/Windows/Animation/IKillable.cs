﻿namespace Modules.Windows.Animation
{
    public interface IKillable
    {
        void Kill();
    }
}