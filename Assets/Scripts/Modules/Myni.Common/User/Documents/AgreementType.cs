namespace Myni.Common.User.Documents
{
    public enum AgreementType : byte
    {
        PrivacyPolicy = 1,
        TermsOfUse = 2,
        Idfa = 4
    }
}