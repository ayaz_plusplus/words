using Newtonsoft.Json;

namespace Myni.Common.App.Property
{
    [System.Serializable]
    public class ObservableProperty<T> where T : struct
    {
        public static implicit operator T(ObservableProperty<T> property) => property.Value;

        public delegate void OnPropertyChangeEvent(T from, T to);

        private event OnPropertyChangeEvent OnPropertyChange;

        [JsonProperty] private T value;

        [JsonIgnore] public T Value
        {
            get => value;
            set
            {
                OnPropertyChange?.Invoke(this.value, value);
                this.value = value;
            }
        }

        public ObservableProperty()
        {
        }

        public ObservableProperty(T initialValue)
        {
            value = initialValue;
        }

        public void AddListener(OnPropertyChangeEvent listener)
        {
            OnPropertyChange += listener;
        }

        public void RemoveListener(OnPropertyChangeEvent listener)
        {
            OnPropertyChange -= listener;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}