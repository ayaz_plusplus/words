﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace StoneMaster.SceneManagement
{
    [CreateAssetMenu(menuName = "HCG/MatchMaster/SceneManagement/Config")]
    public class SceneManagementConfig : SerializedScriptableObject
    {
        [OdinSerialize] private Dictionary<SceneType, SceneAssetWrapper> sceneMap;

        public bool TryGetSceneName(SceneType sceneType, out string sceneName)
        {
            if (sceneMap.TryGetValue(sceneType, out var sceneAssetWrapper))
            {
                sceneName = sceneAssetWrapper.SceneName;
            }

            sceneName = string.Empty;
            return false;
        }

        public string GetSceneName(SceneType sceneType)
        {
            if (sceneMap.ContainsKey(sceneType))
            {
                return sceneMap[sceneType].SceneName;
            }

            throw new Exception($"Couldn't find scene name of type: {sceneType}");
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            foreach (var sceneAsset in sceneMap)
            {
                sceneAsset.Value.UpdateSceneName();
            }
        }
#endif

        [Serializable]
        private class SceneAssetWrapper
        {
            [OdinSerialize] private string sceneName;

            public string SceneName => sceneName;

#if UNITY_EDITOR
            public SceneAsset sceneAsset;
            public void UpdateSceneName()
            {
                if (sceneAsset) sceneName = sceneAsset.name;
            }
#endif
        }
    }
}