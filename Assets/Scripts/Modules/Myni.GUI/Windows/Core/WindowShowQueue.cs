using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Myni.GUI.Windows.Core
{
    public class WindowShowQueue
    {
        private readonly WindowShower windowShower;
        private readonly Queue<Step> queue;
        private WindowBase current;
        private bool canRunWindow = true;

        public WindowShowQueue(WindowShower windowShower)
        {
            this.windowShower = windowShower;
            queue = new Queue<Step>();
        }

        public WindowShowQueue Add<T>(WindowLayer windowLayer) where T : WindowBase
        {
            var step = new Step
            {
                windowType = typeof(T),
                windowLayer = windowLayer
            };
            queue.Enqueue(step);
            return this;
        }

        public async void Run()
        {
            if (canRunWindow == false)
                return;
            canRunWindow = false;

            var step = queue.Dequeue();

            var showAsyncGenericMethod = typeof(WindowShower)
                .GetMethod(nameof(WindowShower.ShowAsync), new Type[] {typeof(WindowLayer)})
                .MakeGenericMethod(step.windowType);

            var task = (Task)showAsyncGenericMethod.Invoke
            (
                windowShower,
                new object[] { step.windowLayer }
            );
            await task;

            var resultProperty = task.GetType().GetProperty("Result");
            current = (WindowBase)resultProperty.GetValue(task);

            if (queue.Count > 0)
                current.OnClose += RunNext;
        }

        private void RunNext()
        {
            current.OnClose -= RunNext;
            canRunWindow = true;
            Run();
        }

        private struct Step
        {
            public Type windowType;
            public WindowLayer windowLayer;
        }
    }
}