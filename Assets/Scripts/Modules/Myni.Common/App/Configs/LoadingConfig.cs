using System.Collections.Generic;
using Myni.Common.App.Loading.Api;
using UnityEngine;

namespace Myni.Common.App.Configs
{
    [CreateAssetMenu(menuName = "HCG/Common/App/Configs/Loading Config")]
    public class LoadingConfig : ScriptableObject
    {
        [SerializeField] private LoadingStage[] loadingStages;

        public IReadOnlyList<LoadingStage> LoadingStages => loadingStages;
    }
}