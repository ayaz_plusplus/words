﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Modules.Myni.GUI.Windows.Configs
{
    [CreateAssetMenu(menuName = "StoneMaster/Core/Configuration/Question Config")]
    public class QuestionConfig : SerializedScriptableObject
    {
        [field: OdinSerialize] public string question;
        [field: OdinSerialize] public string[] answers;
        [field: OdinSerialize] public string mainAnswer;
    }
}