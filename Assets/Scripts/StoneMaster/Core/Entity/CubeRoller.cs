﻿using System;
using System.Collections;
using StoneMaster.Core.Configs;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public class CubeRoller : MonoBehaviour
    {
        [Inject] private GameConfig gameConfig;
        [Inject] private WordChecker wordChecker;
        [Inject] private Board board;
        
        private MovingCube movingCube;
        private bool isMoving;
        
        private void Start()
        {
            movingCube = GetComponent<MovingCube>();
        }

        public void Roll(Vector3 direction)
        {
            if (isMoving)
                return;
            
            isMoving = true;
            var district = board.District;
            var gridDirection = GetGridDirection(direction);
            var row = (int) gridDirection.x;
            var column = (int) gridDirection.y;
  
            if (district.TryGetCellByRowColumn(row, column, out var cell))
            {
                MoveLogic(direction);
                movingCube.CellSetup(cell);
            }
            else isMoving = false;
        }
        
        private void MoveLogic(Vector3 direction)
        {
            StartCoroutine(Rolling(direction));
        }
        
        private IEnumerator Rolling(Vector3 direction)
        {
            var anchor = transform.position + (Vector3.down + direction) * gameConfig.AnchorOffset;
            var axis = Vector3.Cross(Vector3.up, direction);

            for (var i = 0; i < (90 / gameConfig.RoolSpeed); i++)
            {
                transform.RotateAround(anchor, axis, gameConfig.RoolSpeed);
                yield return new WaitForSeconds(0.01f);
            }
            
            movingCube.ResetTopLetterRotate();
            
            yield return new WaitForSeconds(gameConfig.AfterMoveDelay);
            
            wordChecker.SearchWords();
            
          //  movingCube.ResetTopLetterRotate();
            isMoving = false;
        }

        private Vector2 GetGridDirection(Vector3 direction)
        {
            var moveDirection = Vector2.zero;
            var row = movingCube.Cell.Row;
            var column = movingCube.Cell.Column;
            if (direction == Vector3.left)
                moveDirection = new Vector2(row, column - 1);
            
            if (direction == Vector3.right)
                moveDirection = new Vector2(row, column + 1);

            if (direction == Vector3.forward)
                moveDirection = new Vector2(row - 1, column);
            
            if (direction == Vector3.back)
                moveDirection = new Vector2(row + 1, column);
            return moveDirection;
        }

    }
}