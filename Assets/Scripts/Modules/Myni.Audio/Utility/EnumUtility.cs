using System;

namespace Myni.Audio.Utility
{
    public static class EnumUtility
    {
        public static void ForEach<T>(Action<T> action) where T : Enum
        {
            var values = Enum.GetValues(typeof(T));

            foreach (var value in values)
            {
                action.Invoke((T) value);
            }
        }
    }
}