using System.Collections.Generic;
using Myni.Audio.Api.Base;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Myni.Audio.Api
{
    [CreateAssetMenu(menuName = "HCG/Common/Audio/Api/Sounds Scriptable DB")]
    public class SoundsScriptableDb : SerializedScriptableObject, ISoundsDb
    {
        [OdinSerialize] private Dictionary<string, SoundMeta> sounds;

        bool ISoundsDb.TryGetSoundMeta(string id, out SoundMeta soundMeta)
        {
            return sounds.TryGetValue(id, out soundMeta);
        }
    }
}