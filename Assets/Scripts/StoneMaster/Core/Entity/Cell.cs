﻿using UnityEngine;

namespace StoneMaster.Core.Entity
{
    public struct Cell
    {
        public int Row;
        public int Column;
        public Vector3 Position;

        public Cell(int row, int column, Vector3 position)
        {
            Row = row;
            Column = column;
            Position = position;
        }
        
        public Cell(int row, int column)
        {
            Row = row;
            Column = column;
            Position = Vector3.zero;
        }
    }
}