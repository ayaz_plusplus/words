﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StoneMaster.UI
{
    public class TestToggle : MonoBehaviour
    {
        public Toggle toggle;
        public TMP_Text label;
    }
}