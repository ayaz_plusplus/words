﻿using DG.Tweening;

namespace Modules.Windows.Animation
{
    public interface IWindowAnimator : IKillable
    {
        void Show(TweenCallback onShowingAnimationEnd);
        void Hide(TweenCallback yieldClose);
        void Skip();
    }
}