﻿using System.IO;
using Newtonsoft.Json;

namespace Myni.Common.User.Profile.Api.Data
{
    [System.Serializable]
    public abstract class ServerProfileData : ProfileData
    {
        public override void SaveImmediately()
        {
            var path = $"{ProfilePathProvider.GetServerPath()}/{Name}{Profile.DataFormat}";

            IsSaving = true;

            Profile.Save(this, path);

            IsSaving = false;
        }

        public override void Reset()
        {
            OnGenerate();
            Save();
        }

        public override void OnGenerate()
        {
        }

        public override void OnLoaded()
        {
        }
    }
}