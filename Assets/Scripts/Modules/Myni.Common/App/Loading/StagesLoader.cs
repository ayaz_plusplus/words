using System.Collections;
using Myni.Common.App.Configs;
using Myni.Common.App.Services;
using UnityEngine;
using Zenject;

namespace Myni.Common.App.Loading
{
    public class StagesLoader : MonoBehaviour
    {
        [Inject] private LoadingConfig appConfig;
        [Inject] private CoroutineHost coroutineHost;
        [Inject] private DiContainer container;

        private void Start()
        {
            Debug.Log($"StagesLoader");
            coroutineHost.StartCoroutine(LoadStages());
        }

        private IEnumerator LoadStages()
        {
            var loadingStages = appConfig.LoadingStages;
            foreach (var loadingStage in loadingStages)
            {
                container.Inject(loadingStage);
                yield return loadingStage.Load();
                yield return null;
            }
        }
    }
}