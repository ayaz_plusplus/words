﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Myni.GUI.Windows.Core;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Modules.Windows.Configs
{
    [CreateAssetMenu(menuName = "HCG/Common/UI/Windows/Config/User Interface Config")]
    public class UserInterfaceConfig : SerializedScriptableObject, IWindowAnimationConfig
    {
        [SerializeField, BoxGroup("Visuals")] private FadeAnimationConfig fadeAnimationConfig;

        [OdinSerialize, BoxGroup("Technical")] private Dictionary<Type, AssetReferenceGameObject> windowsAssets;

        public FadeAnimationConfig FadeAnimationConfig => fadeAnimationConfig;

        public float AnimationDuration => fadeAnimationConfig.AnimationDuration;
        public Ease Ease => fadeAnimationConfig.Ease;

        public bool TryGetAssetReference(Type type, out AssetReferenceGameObject assetReference)
        {
            return windowsAssets.TryGetValue(type, out assetReference);
        }
#if UNITY_EDITOR
        [Button(ButtonSizes.Gigantic, ButtonStyle.Box, Name = "UPDATE ASSET REFERENCES")]
        [ContextMenu("UPDATE ASSET REFERENCES")]
        private void LoadAssetReferences()
        {
            windowsAssets = new Dictionary<Type, AssetReferenceGameObject>();

            var title = "Updating asset references";
            UnityEditor.EditorUtility.DisplayProgressBar(title, "finding prefabs", 0f);
            string[] guids = UnityEditor.AssetDatabase.FindAssets("t:Prefab");
            UnityEditor.EditorUtility.DisplayProgressBar(title, "loading assets", .1f);

            var step = .8f / guids.Length;
            var progress = .2f;

            foreach (var guid in guids)
            {
                string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                WindowBase asset = UnityEditor.AssetDatabase.LoadAssetAtPath<WindowBase>(assetPath);
                UnityEditor.EditorUtility.DisplayProgressBar(title, $"loading asset at: {assetPath}", progress += step);

                if (asset == null) continue;

                var assetReference = new AssetReferenceGameObject(guid);
                var type = asset.GetType();

                windowsAssets[type] = assetReference;

                Debug.Log($"<color=#33C7FF>[{type.Name}] = {assetReference}</color>");
            }

            UnityEditor.EditorUtility.DisplayDialog(title, "Updating asset references is successful", "ok");
        }
#endif
    }
}