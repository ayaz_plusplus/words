﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Myni.GUI.Windows.Core
{
    public class WindowShower
    {
        [Inject] private WindowBase.Factory windowFactory;
        [Inject] private WindowsContainer windowsContainer;
        [Inject] private IAssetReferenceGameObjectProvider assetReferenceGameObjectProvider;

        private WindowShowQueue windowShowQueue;
        public WindowShowQueue WindowShowQueue
        {
            get
            {
                if (windowShowQueue != null) return windowShowQueue;
                windowShowQueue = new WindowShowQueue(this);
                return windowShowQueue;

            }
            private set => windowShowQueue = value;
        }

        public WindowShowSequence GetSequence()
        {
            return new WindowShowSequence(this);
        }

        public WindowShowSequence RunSequence()
        {
            return GetSequence().RunNextFrame();
        }

        public WindowShowQueue GetQueue()
        {
            return new WindowShowQueue(this);
        }

        public async Task<T> ShowAsync<T>(WindowLayer windowLayer) where T : WindowBase
        {
            var window = await ShowAsyncInternal<T>();

            windowsContainer.Add(windowLayer, window);
            window.Show();

            return window;
        }

        public async Task<T> ShowAsyncRect<T>(RectTransform holder) where T : WindowBase
        {
            var window = await ShowAsyncInternal<T>();

            windowsContainer.Add(holder, window);
            window.Show();

            return window;
        }
        
        private async Task<T> ShowAsyncInternal<T>() where T : WindowBase
        {
            var type = typeof(T);
            var hashCode = type.GetHashCode();

            if (windowsContainer.TryGetWindow(hashCode, out var windowBase))
            {
                return windowBase as T;
            }

            GameObject prefab = default;

            if (assetReferenceGameObjectProvider.TryGet(typeof(T), out var assetReferenceGameObject))
            {
                if (assetReferenceGameObject.IsValid())
                {
                    prefab = (GameObject) await assetReferenceGameObject.OperationHandle.Task;
                }
                else
                {
                    var asyncOperation = assetReferenceGameObject.LoadAssetAsync();
                    await asyncOperation.Task;
                    prefab = asyncOperation.Result;
                }
            }

            if (prefab == null)
            {
                Debug.LogError($"PREFAB IS NULL! {type}");
                return null;
            }

            var windowInstance = windowFactory.Create(prefab.GetComponent<WindowBase>());
            var window = windowInstance as T;

            if (window == null)
            {
                Debug.LogError($"Couldn't cast {windowInstance} as {type.Name}");
                Object.Destroy(windowInstance.gameObject);
                return null;
            }

            return window;
        }
        
        public void Close(WindowBase window)
        {
            windowsContainer.Remove(window);
        }

        public void OnClosed(WindowBase window)
        {
            Object.Destroy(window.gameObject);

            if (assetReferenceGameObjectProvider.TryGet(window.GetType(), out var assetReferenceGameObject) &&
                !assetReferenceGameObject.IsValid())
            {
                assetReferenceGameObject.ReleaseAsset();
            }
        }
    }
}