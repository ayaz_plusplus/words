﻿namespace Myni.Common.App.Services.Random
{
    public interface IRandomizer
    {
        int Get(int min, int max);
    }
}