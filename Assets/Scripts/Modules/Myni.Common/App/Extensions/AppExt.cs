namespace Myni.Common.App.Extensions
{
    public static class AppExt
    {
        public static bool IsDevelopmentBuild()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            return true;
#else
            return false;
#endif
        }

        public static bool IsProductionBuild()
        {
#if PRODUCTION
            return true;
#else
            return false;
#endif
        }
    }
}