﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Modules.Windows.Configs;
using UnityEngine;

namespace Myni.GUI.Windows.Core.Animation
{
    public class PopWindowAnimator : IWindowAnimator
    {
        private readonly CanvasGroup canvasGroup;
        private readonly IWindowAnimationConfig config;
        private readonly RectTransform rectTransform;

        private TweenerCore<float, float, FloatOptions> tween;

        private float startAlpha;

        private float startScale;
        private float targetAlpha;
        private float targetScale;

        private float CurrentValue { get; set; }

        public PopWindowAnimator(RectTransform rectTransform, IWindowAnimationConfig config, CanvasGroup canvasGroup)
        {
            this.rectTransform = rectTransform;
            this.config = config;
            this.canvasGroup = canvasGroup;
        }

        void IWindowAnimator.Show(TweenCallback callback)
        {
            CurrentValue = 0f;

            startAlpha = 0f;
            targetAlpha = 1f;

            startScale = 0f;
            targetScale = 1f;

            DoTween(callback);
        }

        void IWindowAnimator.Hide(TweenCallback callback)
        {
            CurrentValue = 0f;

            startAlpha = 1f;
            targetAlpha = 0f;

            startScale = 1f;
            targetScale = 0f;

            DoTween(callback);
        }

        private void DoTween(TweenCallback onComplete)
        {
            tween?.Kill();
            SetCurrentValue(GetCurrentValue());
            tween = DOTween.To(GetCurrentValue, SetCurrentValue, 1f, config.AnimationDuration)
                .SetUpdate(true)
                .SetEase(config.Ease)
                .OnComplete(onComplete)
                .Play();
        }

        private float GetCurrentValue()
        {
            return CurrentValue;
        }

        private void SetCurrentValue(float value)
        {
            CurrentValue = value;
            canvasGroup.alpha = Mathf.Lerp(startAlpha, targetAlpha, CurrentValue);
            rectTransform.localScale = Vector3.one * Mathf.Lerp(startScale, targetScale, CurrentValue);
        }

        public void Skip()
        {
            tween?.Complete();
        }         
        
        void IKillable.Kill()
        {
            tween?.Kill();
        }
    }
}