﻿using System;
using Lean.Touch;
using StoneMaster.Core.Configs;
using StoneMaster.Signals;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public class MovingCube : SceneCube
    {
        [SerializeField]
        private Rigidbody rigidbody;

        [SerializeField]
        private MeshRenderer meshRenderer;

        [SerializeField]
        private GameObject selectedFrame;
        
        [Inject] private GameConfig gameConfig;
        [Inject] private SignalBus signalBus;

        public bool Selected { get; private set; }

        private CubeRoller cubeRoller;

        private void OnEnable()
        {
            LeanTouch.OnFingerSwipe += HandleFingerSwipe;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerSwipe -= HandleFingerSwipe;
        }

    
        private void HandleFingerSwipe(LeanFinger finger)
        {
            var screenTo =  finger.StartScreenPosition;
            var screenFrom =     finger.ScreenPosition;
            var finalDelta = screenTo - screenFrom;
            
            Vector3 moveVector = Vector3.zero;
            
            if (finalDelta.x < -Mathf.Abs(finalDelta.y)) moveVector = Vector3.right;
            if (finalDelta.x >  Mathf.Abs(finalDelta.y)) moveVector =  Vector3.left;
            if (finalDelta.y < -Mathf.Abs(finalDelta.x)) moveVector = Vector3.forward;
            if (finalDelta.y >  Mathf.Abs(finalDelta.x)) moveVector =  Vector3.back;
            
            MoveLogic(moveVector);
            Debug.Log(finalDelta);
        }

        protected void Start()
        {
            cubeRoller = GetComponent<CubeRoller>();
            UnSelect();
        }
        
        private void Update()
        {
            if (Selected == false)
                return;
            if (Input.GetKeyDown(KeyCode.A)) MoveLogic(Vector3.left);
            if (Input.GetKeyDown(KeyCode.D)) MoveLogic(Vector3.right);
            if (Input.GetKeyDown(KeyCode.W)) MoveLogic(Vector3.forward);
            if (Input.GetKeyDown(KeyCode.S)) MoveLogic(Vector3.back);

        }

        private void Select()
        {
            Selected = true;
            selectedFrame.SetActive(true);
            signalBus.Fire(new CoreGameSignal.LetterSelectedSignal(this));
        }

        public void UnSelect()
        {
            Selected = false;
            selectedFrame.SetActive(false);
            signalBus.Fire(new CoreGameSignal.LetterUnselectedSignal(this));
        }

        public void OnTouchCube()
        {
            if (Selected)
                UnSelect();
            else
                Select();
        }

        private void MoveLogic(Vector3 direction)
        {
            if (Selected == false)
                return;
            
            cubeRoller.Roll(direction);
        }

        public class Factory : PlaceholderFactory<MovingCube>
        {
        }
    }
}