﻿using UnityEngine;

namespace Myni.Common.Extensions
{
    public static class RectTransformExtension
    {
        public static void StretchToParent(this RectTransform rectTransform)
        {
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.localScale = Vector3.one;
        }

        public static void Reset(this RectTransform rectTransform)
        {
            rectTransform.anchorMax = Vector2.one * 0.5f;
            rectTransform.anchorMin = Vector2.one * 0.5f;
            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.localScale = Vector3.one;           
        }

        public static void SetAnchoredXPosition(this RectTransform rectTransform, float x)
        {
            var anchoredPosition = rectTransform.anchoredPosition;
            anchoredPosition.x = x;
            rectTransform.anchoredPosition = anchoredPosition;
        }
        
        public static void SetAnchoredYPosition(this RectTransform rectTransform, float y)
        {
            var anchoredPosition = rectTransform.anchoredPosition;
            anchoredPosition.y = y;
            rectTransform.anchoredPosition = anchoredPosition;
        }
    }
}