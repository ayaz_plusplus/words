﻿using StoneMaster.Core.Configs;
using StoneMaster.SceneManagement;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Installers
{
    [CreateAssetMenu(menuName = "HCG/Installers/Configs Installer")]
    public class ConfigsInstaller: ScriptableObjectInstaller<ConfigsInstaller>
    {
        [SerializeField] private SceneManagementConfig sceneManagementConfig;
 
        public override void InstallBindings()
        {
            Container.BindInstance(sceneManagementConfig);
        }
    }
}