﻿using System;
using UnityEngine;

namespace Myni.Common.Utility.FlyValue
{
    public abstract class FlyValueBase<T> : ScriptableObject
    {
        [SerializeField] private T startValue;

        public event Action<T> OnValueChanged;
        
        private T currentValue;

        public T CurrentValue
        {
            get => currentValue;
            set
            {
                currentValue = value;
                OnValueChanged?.Invoke(currentValue);
            }
        }

        private void OnEnable()
        {
            currentValue = startValue;
        }
    }
}