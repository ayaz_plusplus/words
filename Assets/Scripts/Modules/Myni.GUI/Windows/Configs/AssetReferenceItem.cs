﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Modules.Windows.Configs
{
    [System.Serializable]
    public class AssetReferenceItem
    {
        [SerializeField] public Type windowType;
        [SerializeField] public AssetReferenceGameObject assetReference;
    }
}