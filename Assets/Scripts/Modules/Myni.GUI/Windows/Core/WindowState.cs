﻿namespace Myni.GUI.Windows.Core
{
    public enum WindowState
    {
        Opening,
        Opened,
        Closing,
        Closed,
        Hiding,
        Hidden
    }
}