﻿namespace Myni.Common.App.Services.Random
{
    public class SystemRandomizer : IRandomizer
    {
        private readonly System.Random random;

        public SystemRandomizer()
        {
            random = new System.Random();
        }

        public int Get(int min, int max)
        {
            var num = random.Next(min, max);
            return num;
        }
    }
}