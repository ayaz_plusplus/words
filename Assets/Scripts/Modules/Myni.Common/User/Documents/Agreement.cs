using System;
using Newtonsoft.Json;

namespace Myni.Common.User.Documents
{
    [Serializable]
    public class Agreement
    {
        [JsonProperty] private bool userAnswered;
        [JsonProperty] private bool sign;
        [JsonProperty] private long timestamp;

        public void Sign(bool value)
        {
            userAnswered = true;
            sign = value;
            timestamp = GetUtcNowTimestamp();
        }

        public bool HasUserAnswered() => userAnswered;
        public bool GetSign() => sign;
        public long GetTimestamp() => timestamp;

        private static long GetUtcNowTimestamp() => (long)
            DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
    }
}