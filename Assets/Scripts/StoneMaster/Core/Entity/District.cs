﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace StoneMaster.Core.Entity
{
    public class District : MonoBehaviour
    {
        [SerializeField]
        private Transform[] rows;

        private Board board;
        private Dictionary<int, Dictionary<int, Cell>> cells = new Dictionary<int, Dictionary<int, Cell>>();

        public Dictionary<int, Dictionary<int, Cell>> Cells => cells;

        public void Initialize(Board board)
        {
            for (int rowIndex = 0; rowIndex < rows.Length; rowIndex++)
            {
                var row = rows[rowIndex];
                var cellsAtRow = new Dictionary<int, Cell>();

                for (int column = 0; column < row.childCount; column++)
                {
                    var cellTransform = row.GetChild(column);
                    var cellPos = cellTransform.position;
                    var offsetPos = new Vector3(cellPos.x, cellPos.y, -0.5f);
                    Cell cell = new Cell(rowIndex, column, offsetPos);
                    
                    if (cellTransform.childCount > 0)
                    {
                        var cubeBase = cellTransform.GetChild(0)
                            .GetComponent<ConstructorCube>();
                        cubeBase.gameObject.SetActive(false);
                        board.CreateLetter(cell, cellTransform, cubeBase.cubeType, cubeBase.LettersContainer);
                    }
                    
                    cellsAtRow[column] = cell;
                   
                }

                cells[rowIndex] = cellsAtRow;
            }

            this.board = FindObjectOfType<Board>();
        }

        public bool TryGetCellByRowColumn(int row, int column, out Cell cell)
        {
            cell = new Cell(row, column);
            if ((cells.ContainsKey(row) && cells[row].ContainsKey(column)) == false)
                return false;

            if (board.Letters.Any(letter => letter.Cell.Row == row && letter.Cell.Column == column))
                return false;

            cell = cells[row][column];
            return true;
        }

    }
}