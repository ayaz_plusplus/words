namespace Myni.Audio.Api
{
    [System.Serializable]
    public enum SoundChannel
    {
        Music,
        Sfx
    }
}