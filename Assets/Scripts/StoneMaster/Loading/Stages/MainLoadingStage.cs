using System.Collections;
using Myni.Common.App.Loading.Api;
using Myni.Common.App.Services;
using StoneMaster.SceneManagement;
using UnityEngine;
using Zenject;

namespace StoneMaster.Loading.Stages
{
    [CreateAssetMenu(menuName = "HCG/Common/Loading/Stages/Main Loading Stage")]
    public class MainLoadingStage : LoadingStage
    {
        [Inject] private SceneSwitcher sceneSwitcher;
        [Inject] private SceneManagementConfig sceneManagementConfig;
    
        public override IEnumerator Load()
        {
            StartLoad();


            yield return sceneSwitcher.LoadSceneAsync(sceneManagementConfig.GetSceneName(SceneType.CoreGame));
            EndLoad();
        }
    }
}
