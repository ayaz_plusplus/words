using System.IO;
using Myni.Common.App.Extensions;

namespace Myni.Common.User.Profile.Api
{
    public static class ProfilePathProvider
    {
        private static string localPath;
        private static string serverPath;

        public static string GetLocalPath() =>
            string.IsNullOrWhiteSpace(localPath) ? localPath = GetPath("profile/local") : localPath;

        public static string GetServerPath() =>
            string.IsNullOrWhiteSpace(serverPath) ? serverPath = GetPath("profile/server") : serverPath;

        private static string GetPath(string folder)
        {
            var persistentPath = AppDataPath.GetPersistentPath();
            var fullPath = persistentPath + $@"/{folder}";

            if (!Directory.Exists(persistentPath)) Directory.CreateDirectory(persistentPath);
            if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);

            return fullPath;
        }
    }
}