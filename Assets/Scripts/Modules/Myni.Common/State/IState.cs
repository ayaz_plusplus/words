﻿namespace Myni.Common.State
{
    public interface IState
    {
        void Enter();
        void Update();
        void Exit();
    }
}