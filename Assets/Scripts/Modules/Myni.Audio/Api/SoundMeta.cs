using UnityEngine;

namespace Myni.Audio.Api
{
    [System.Serializable]
    public class SoundMeta
    {
        [SerializeField] private AudioClip audioClip;
        [SerializeField] private bool loop;
        [SerializeField, Range(0f, 1f)] private float volume = .75f;
        [SerializeField, Range(-3f, 3f)] private float pitch = 1f;

        public AudioClip AudioClip => audioClip;
        public bool Loop => loop;
        public float Volume => volume;
        public float Pitch => pitch;
    }
}