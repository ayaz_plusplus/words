﻿using Newtonsoft.Json;
using UnityEngine;

namespace Myni.Common.User.Profile.Api.Data
{
    public class LevelProgressData : LocalProfileData
    {
        [JsonProperty("currentLevel")] public int CurrentLevel { get; private set; }
        
        public override void OnGenerate()
        {
            Debug.Log("generated");
        }

        public override void OnLoaded()
        {
            
        }
        
        public void IncreaseLevel()
        {
            CurrentLevel++;
            Save();
        }
    }
}