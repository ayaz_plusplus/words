﻿using UnityEngine;

namespace Myni.GUI.Components
{
    [RequireComponent(typeof(RectTransform))]
    public class ProgressBarUI : MonoBehaviour
    {
        [SerializeField] private RectTransform mask;
        [SerializeField] [Range(0, 1)] private float value;
        [SerializeField] private RectTransform barRectTransform;

        public float Value => value;

        private void Start()
        {
            UpdateMask();
        }

        private void OnValidate()
        {
            UpdateMask();
        }

        private void UpdateMask()
        {
            var sizeDelta = mask.sizeDelta;
            sizeDelta.x = value * barRectTransform.rect.width;
            mask.sizeDelta = sizeDelta;
        }

        public void SetValue(float value)
        {
            this.value = Mathf.Clamp01(value);
            UpdateMask();
        }
    }
}