using System;
using System.Collections.Generic;
using Windows;
using Myni.GUI.Windows.Core;
using StoneMaster.Core.GameStates;
using UnityEngine;
using Zenject;

namespace HCG.MatchMaster.Core.GameStates.States
{
    public class GameMetaIdleState : GameState
    {
        [Inject] private WindowsContainer windowsContainer;
        [Inject] private WindowShower windowShower;

        public GameMetaIdleState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }

        public override async void Enter()
        {
            await windowShower.ShowAsync<MenuWindow>(WindowLayer.Main);
        }

        public override void Update()
        {
        }

        public override void Exit()
        {

        }

        public class Factory : PlaceholderFactory<GameStateMachine, GameMetaIdleState>
        {
        }
    }

    public class PostGameWindows : IDisposable
    {
        private readonly IAnimationLauncher animationLauncher;

        private Queue<IWindowTrigger> windowTriggers;
        private WindowBase currentWindow;
        private IWindowTrigger currentTrigger;

        public PostGameWindows(IAnimationLauncher animationLauncher, IWindowTrigger[] windowTriggers)
        {
            this.windowTriggers = new Queue<IWindowTrigger>();
            foreach (var windowTrigger in windowTriggers)
                this.windowTriggers.Enqueue(windowTrigger);

            this.animationLauncher = animationLauncher;
            this.animationLauncher.AnimationDone += StartNextTrigger;
        }

        private void StartNextTrigger()
        {
            if (windowTriggers.Count == 0) return;

            currentTrigger = windowTriggers.Dequeue();
            currentTrigger.OnSuccess += HandleCurrentWindow;
            currentTrigger.OnFail += SkipCurrentWindow;
            currentTrigger.Trigger();
        }

        private void HandleCurrentWindow(WindowBase window)
        {
            currentTrigger.OnSuccess -= HandleCurrentWindow;
            currentTrigger.OnFail -= SkipCurrentWindow;

            if (currentWindow != null)
                currentWindow.OnClose -= StartNextTrigger;

            currentWindow = window;
            currentWindow.OnClose += StartNextTrigger;
        }

        private void SkipCurrentWindow()
        {
            currentTrigger.OnSuccess -= HandleCurrentWindow;
            currentTrigger.OnFail -= SkipCurrentWindow;

            StartNextTrigger();
        }

        public void Dispose()
        {
            animationLauncher.AnimationDone -= StartNextTrigger;
        }
    }

    public interface IAnimationLauncher
    {
        event Action AnimationDone;
    }

    public interface IWindowTrigger
    {
        event Action<WindowBase> OnSuccess;
        event Action OnFail;
        void Trigger();
    }
}