using UnityEngine;

namespace Myni.Audio.Api.Base
{
    public interface IAudioSourceProvider
    {
        bool TryGetAudioSource(SoundChannel soundChannel, out AudioSource audioSource);
    }
}