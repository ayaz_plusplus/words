﻿using System.Collections.Generic;
using UnityEngine;

namespace Myni.Audio.Api
{
    public class AudioSourceCollection : List<AudioSource>
    {
        public AudioSourceCollection(AudioSource audioSource)
        {
            Add(audioSource);
        }
    }
}