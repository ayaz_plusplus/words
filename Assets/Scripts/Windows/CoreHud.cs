﻿using Myni.GUI.Windows.Core;
using StoneMaster.Core.Entity;
using StoneMaster.Signals;
using TMPro;
using UnityEngine;
using Zenject;

namespace Windows
{
    public class CoreHud : WindowBase
    {
        private Board board;

        [SerializeField] private TMP_Text word;

        public TMP_Text Word => word;

        [Inject] private SignalBus signalBus;
        public void Restart()
        {
            signalBus.Fire<RestartedSignal>();
            board = FindObjectOfType<Board>();
            board.Clear();
            board.Initialize(this);
        
        }
    }
}