﻿using UnityEngine;

namespace Myni.Common.App.Extensions
{
    public static class AppDataPath
    {
        public static string GetPersistentPath()
        {
#if PRODUCTION
            return UnityEngine.Application.persistentDataPath;
#else
            return Application.persistentDataPath + @"/development";
#endif
        }
    }
}