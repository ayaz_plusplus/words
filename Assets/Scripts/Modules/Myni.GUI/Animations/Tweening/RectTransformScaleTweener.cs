﻿using UnityEngine;

namespace Myni.GUI.Animations.Tweening
{
    public class RectTransformScaleTweener : BasicTweenAnimator
    {
        [SerializeField] private RectTransform targetRectTransform;
        [SerializeField] private Vector3 scaleFrom;
        [SerializeField] private Vector3 scaleTo;

        protected override void Animate(float t)
        {
            targetRectTransform.localScale = Vector3.Lerp(scaleFrom, scaleTo, t);
        }
    }
}