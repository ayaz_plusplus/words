using Myni.Audio.Api.Base;
using UnityEngine;
using Zenject;

namespace Myni.Audio.Api
{
    public class AudioPlayer : IAudioPlayer
    {
        [Inject] private ISoundsDb soundsDb;
        [Inject] private IAudioSourceProvider audioSourceProvider;
        [Inject] private IAudioMixerGroupProvider audioMixerGroupProvider;

        void IAudioPlayer.Play(string id, SoundChannel soundChannel)
        {
#if DEBUG_AUDIO
            Debug.Log($"Play {soundChannel}: {id}");
#endif

            if (soundsDb.TryGetSoundMeta(id, out var soundMeta))
            {
                if (audioSourceProvider.TryGetAudioSource(soundChannel, out var audioSource))
                {
                    ApplySoundMetaTo(soundMeta, audioSource, soundChannel);
                    audioSource.Play();
                }
                else
                {
                    Debug.LogError($"Couldn't get any available {soundChannel} AudioSource");
                }
            }
            else
            {
                Debug.LogError($"Couldn't find sound with id {id} in {soundsDb}");
            }
        }

        bool IAudioPlayer.Stop(string id, SoundChannel soundChannel)
        {
            return false;
        }

        private void ApplySoundMetaTo(SoundMeta soundMeta, AudioSource audioSource, SoundChannel soundChannel)
        {
            audioSource.clip = soundMeta.AudioClip;
            audioSource.loop = soundMeta.Loop;
            audioSource.pitch = soundMeta.Pitch;
            audioSource.volume = soundMeta.Volume;

            if (audioMixerGroupProvider.TryGet(soundChannel, out var audioMixerGroup))
            {
                audioSource.outputAudioMixerGroup = audioMixerGroup;
            }
        }
    }
}