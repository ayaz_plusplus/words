using System.IO;
using Myni.Common.User.Profile.Api;
using UnityEditor;
using UnityEngine;

namespace Myni.Common.Editor
{
    public static class ToolProfile
    {
        private const string Tab = "/Profile";

        [MenuItem(CustomTools.ToolsTab + Tab + "/Delete PlayerPrefs")]
        private static void DeletePlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        [MenuItem(CustomTools.ToolsTab + Tab + "/Reset")]
        private static void Bootstrap()
        {
            DeleteDirectory(ProfilePathProvider.GetLocalPath());
            DeleteDirectory(ProfilePathProvider.GetServerPath());
        }

        private static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }
    }
}