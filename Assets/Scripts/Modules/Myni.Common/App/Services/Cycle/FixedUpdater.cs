﻿using UnityEngine;

namespace Modules.Myni.Common.App.Services.Cycle
{
    public class FixedUpdater : BaseUpdater<IFixedUpdatable>
    {
        private void FixedUpdate()
        {
            DeltaTime = Time.fixedDeltaTime;

            foreach (var updatable in updatables)
            {
                if (updatable.IsDestroyed) continue;
                updatable.FixedUpdateManually(DeltaTime);
            }

            ProcessUnregisterQueue();
            ProcessRegisterQueue();
        }
    }
}