﻿using System;
using Myni.GUI.Animations.Tweening;
using UnityEngine;

namespace HCG.Common.UI.Animations.Tweening
{
    public class CanvasGroupTweener : BasicTweenAnimator
    {
        [SerializeField] private CanvasGroup target;
        [SerializeField] private float fromAlpha;
        [SerializeField] private float toAlpha;

        protected override void Animate(float t)
        {
            target.alpha = Mathf.Lerp(fromAlpha, toAlpha, t);
        }
    }
}