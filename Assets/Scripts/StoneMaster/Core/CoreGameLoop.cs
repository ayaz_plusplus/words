﻿using System.Threading.Tasks;
using HCG.MatchMaster.Core.GameStates.States;
using Modules.Myni.Common.App.Services.Cycle;
using Myni.GUI.Windows.Core;
using StoneMaster.Core.GameStates;
using StoneMaster.Core.GameStates.Pause;
using StoneMaster.Signals;
using Zenject;

namespace StoneMaster.Core
{
      public class CoreGameLoop : UpdatableMonobehaviour, IRegularUpdatable
    {
        [Inject] private SignalBus signalBus;
        [Inject] private GameStateMachine.Factory gameStateMachineFactory;

        [Inject] private IUpdater<IRegularUpdatable> updater;
        [Inject] private IPauseHandler pauseHandler;

        [Inject] private WindowShower windowShower;

        private GameStateMachine gameStateMachine;

        private void Awake()
        {
            gameStateMachine = gameStateMachineFactory.Create();
            gameStateMachine.Initialize();
        }

        private void OnEnable()
        {
            Subscribe();
        }

        private void OnDisable()
        {
            Unsubscribe();
        }

        public void UpdateManually(float deltaTime)
        {
            gameStateMachine.Update();
        }

        private void GoToPlay()
        {
            gameStateMachine.SetState<GamePlayingState>();
        }
        
        private async void GoToMeta()
        {
            gameStateMachine.SetState<GameLevelClearState>();
            await Task.Yield();
            gameStateMachine.SetState<GameMetaIdleState>();
        }

        private void OnPlayButton()
        {

        }

        private void OnResumeGame()
        {
            updater.Register(this);
        }

        private void OnPauseGame()
        {
            updater.Unregister(this);
        }
        
        private void Subscribe()
        {
            signalBus.Subscribe<GoToMetaSignal>(GoToMeta);
            signalBus.Subscribe<GoToPlaySignal>(GoToPlay);
        }

        private void Unsubscribe()
        {
            signalBus.Unsubscribe<GoToMetaSignal>(GoToMeta);
            signalBus.Unsubscribe<GoToPlaySignal>(GoToPlay);
        }
    }
}