﻿using Myni.Common.App.Loading.Api;
using StoneMaster.Signals;
using Zenject;

namespace StoneMaster.Core.Installers
{
    public class SignalsInstaller : MonoInstaller<SignalsInstaller>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<StageLoadedSignal>().OptionalSubscriber();
            Container.DeclareSignal<GoToMetaSignal>().RequireSubscriber();
            Container.DeclareSignal<GoToPlaySignal>().RequireSubscriber();
            Container.DeclareSignal<RestartedSignal>().RequireSubscriber();
            CoreSignalInstall();
            
        }

        private void CoreSignalInstall()
        {
            Container.DeclareSignal<CoreGameSignal.LetterSelectedSignal>().RequireSubscriber();
            Container.DeclareSignal<CoreGameSignal.LetterUnselectedSignal>().RequireSubscriber();
        }
    }
}