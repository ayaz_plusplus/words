﻿using UnityEngine;

namespace Myni.GUI.Components
{
    [RequireComponent(typeof(RectTransform))]
    public class Spinner : MonoBehaviour
    {
        [SerializeField] private float speed;

        private RectTransform rectTransform;

        private void Awake()
        {
            enabled = TryGetComponent(out rectTransform);
        }

        private void LateUpdate()
        {
            rectTransform.Rotate(0f, 0f, speed * Time.deltaTime);
        }
    }
}