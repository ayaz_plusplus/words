﻿namespace Myni.Common.State
{
    public interface IStateMachine
    {
        IState CurrentState { get; }

        void Initialize(IState state);
        void Update();
        void SetState(IState state);
    }
}