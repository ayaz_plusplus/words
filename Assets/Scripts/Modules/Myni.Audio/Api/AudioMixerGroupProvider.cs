using System.Collections.Generic;
using Myni.Audio.Api.Base;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Audio;

namespace Myni.Audio.Api
{
    [CreateAssetMenu(menuName = "HCG/Common/Audio/Api/Audio Mixer Group Provider")]
    public class AudioMixerGroupProvider : SerializedScriptableObject, IAudioMixerGroupProvider
    {
        [OdinSerialize] private Dictionary<SoundChannel, AudioMixerGroup> soundChannelAudioMixerDictionary;

        public bool TryGet(SoundChannel soundChannel, out AudioMixerGroup audioMixerGroup)
        {
            return soundChannelAudioMixerDictionary.TryGetValue(soundChannel, out audioMixerGroup);
        }
    }
}